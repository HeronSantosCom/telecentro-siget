<?php
	// include.configuracao.php

	// inicia sess�o
	session_start();

	// vari�veis
	$sistemanome = "Sistema de Gerenciamento dos Telecentros de Nova Igua�u";
	$sistemabreviatura = "SiGeT";
	$sistemaversao = "1.2a";
	$codigotelecentro = "999";
	$db_hostname = "localhost:3306";
	$db_username = "root";
	//$db_password = substr(md5("root"),0,12);
	$db_password = "";
	$db_databasename = "siget";
	$dataatual = date("d-m-Y");
	$horaatual = date("H:i");
	
	// define as abas do menu
	$menutitulo	= array("P�gina Inicial",	"Perfil de Login",	"Estagi�rio",	"Turma",	"Usu�rio",	"Encerrar Sess�o");
	$menulink	= array("inicial",			"perfildelogin",		"estagiario",	"turma",	"usuario",	"logoff");
	$menunivel	= array("4",					"1",						"2",				"3",		"3",			"4");
	
	// requisita variaveis
	$paginamatriz = $_REQUEST['pm'];
	$paginasub = $_REQUEST['ps'];
	
	// define p�gina atual
	if (empty($paginamatriz)) {
		$paginamatriz = "inicial";
	}
	
	// conecta ao banco de dados
	$db_conexao = mysql_pconnect($db_hostname,$db_username,$db_password) or die(exibeerro("Erro ao efetuar conex�o com o MySQL!<br>Contacte o Administrador!"));
	mysql_select_db($db_databasename,$db_conexao) or die(exibeerro("Erro ao conectar ao banco de dados!<br>Contacte o Administrador!"));
	
	// verifica se esta logado
	if ((!isset($_SESSION["id_acessonivel"])) and (!isset($_SESSION["usuario"]) and (!isset($_SESSION["senha"])))) {
		$paginamatriz = "login";
		$statuslogin = "off";
	} elseif ($paginamatriz == "login") {
		$autenticacao_acessonivel = $_SESSION["id_acessonivel"];
		$autenticacao_usuario = $_SESSION["usuario"];
		$autenticacao_senha = $_SESSION["senha"];
		$paginamatriz = "inicial";		
	}
	
	// define o t�tulo da p�gina matriz a ser aberta
	switch($paginamatriz) {
		case "login": $titulopagina = "Autentica��o"; break;
		case "inicial": $titulopagina = "P�gina Inicial"; break;
		case "estagiario": $titulopagina = "Estagi�rio";  break;	
		case "perfildelogin": $titulopagina = "Perfil de Login";  break;	
		case "turma": $titulopagina = "Turma"; break;
		case "usuario": $titulopagina = "Usu�rio"; break;
		case "logoff": $titulopagina = "Sair do Sistema";  break;
	}
	
	// define o t�tulo da sub p�gina a ser aberta
	switch($paginasub) {
		case "cadastro": $titulosubpagina = "Cadastrando..."; break;
		case "cadastro.post": $titulosubpagina = "Confirma��o..."; break;

		case "consulta": $titulosubpagina = "Consultando..."; break;
		case "consulta.result": $titulosubpagina = "Resultado da consulta..."; break;

		case "edita": $titulosubpagina = "Editando..."; break;
		case "edita.post": $titulosubpagina = "Confirma��o..."; break;

		case "lista": $titulosubpagina = "Listando..."; break;
		
		case "diariodeclasse": $titulosubpagina = "Di�rio de Classe..."; break;
		case "diariodeclasse.post": $titulosubpagina = "Gerando Di�rio de Classe..."; break;

		case "remove": $titulosubpagina = "Removendo..."; break;
		case "remove.post": $titulosubpagina = "Confirma��o..."; break;
		
		case "autenticacao": $titulosubpagina = "Criando autentica��o..."; break;
	}
	
	// carrega a pagina a ser aberta
	if (!empty($paginasub) && !empty($paginamatriz)) {
		$carregapagina = "modulos/$paginamatriz/$paginasub.php";
	} elseif (empty($paginasub) && !empty($paginamatriz)) {
		$carregapagina = "modulos/$paginamatriz.php";
	} else {
		$titulopagina = "Erro";
		$carregapagina = "modulos/erro.php";
	}
	
	// cria o proximo registro
	function proximoregistro($dbColuna,$dbTabela) {
		global $db_conexao; // resgata a variavel db_conexao
		$rowsTabela = mysql_num_rows(mysql_query("select * from " . trim($dbTabela), $db_conexao));
		if ($rowsTabela > 0) { // se existe algum registro
			$selectMax = "select max(". $dbColuna .") + 1 as totaltabela from ". $dbTabela ." where 1";
			$queryMax = mysql_query($selectMax, $db_conexao);
			while (is_array($record = mysql_fetch_array($queryMax))) { //define o valor maximo
				$valor = $record["totaltabela"];
			}
		} else { // n�o existe nenhum registro define o valor 1
			$valor = 1;
		}
		return $valor;
	}
	
	// conversor de data
	function conversordata($data, $separador, $formato) {
		switch($formato) {
			// converte de mysql para normal
			case "mysql.normal":
				$data = substr("$data", 8, 2) . $separador . substr("$data", 5, 2) . $separador . substr("$data", 0, 4); break;
			// converte de normal para mysql
			case "normal.mysql":
				$data = substr("$data", 6, 4) . $separador . substr("$data", 3, 2) . $separador . substr("$data", 0, 2); break;
			// mantem os aspectos do mysql afim de remover o separador
			case "mysql.mysql":
				$data = substr("$data",0,4) . $separador . substr("$data", 5, 2) . $separador . substr("$data", 8, 2); break;
		}
		return $data;
	}

	// conversor para extenso
	function conversorextenso($valor, $formato) {
		// semana: numero para extenso
		if ($formato == "num.semana") {
			switch($valor) {
				case "1": $valor="domingo"; break;
				case "2": $valor="segunda-feira"; break;
				case "3": $valor="ter&ccedil;a-feira"; break;
				case "4": $valor="quarta-feira"; break;
				case "5": $valor="quinta-feira"; break;
				case "6": $valor="sexta-feira"; break;
				case "7": $valor="s&aacute;bado"; break;
			}
		}
		// mes: numero para extenso
		if ($formato == "num.mes") {
			switch($valor) {
				case "1": $valor="janeiro"; break;
				case "2": $valor="fevereiro"; break;
				case "3": $valor="mar&ccedil;o"; break;
				case "4": $valor="abril"; break;
				case "5": $valor="maio"; break;
				case "6": $valor="junho"; break;
				case "7": $valor="julho"; break;
				case "8": $valor="agosto"; break;
				case "9": $valor="setembro"; break;
				case "10": $valor="outubro"; break;
				case "11": $valor="novembro"; break;
				case "12": $valor="dezembro"; break;
			}
		}
		// semana curta: extenso para numero
		if ($formato == "semanacurta.num") {
			switch($valor) {
				case "Sun": $valor=1; break;
				case "Mon": $valor=2; break;
				case "Tue": $valor=3; break;
				case "Wed": $valor=4; break;
				case "Thu": $valor=5; break;
				case "Fri": $valor=6; break;
				case "Sat": $valor=7; break;
			}
		}
		// semana curta: semana extenso
		if ($formato == "semanacurta.semana") {
			switch($valor) {
				case "Sun": $valor="domingo"; break;
				case "Mon": $valor="segunda-Feira"; break;
				case "Tue": $valor="ter&ccedil;a-feira"; break;
				case "Wed": $valor="quarta-feira"; break;
				case "Thu": $valor="quinta-feira"; break;
				case "Fri": $valor="sexta-feira"; break;
				case "Sat": $valor="s&aacute;bado"; break;
			}
		}
		return $valor;
	}
	
	// cria titulo
	function topicodestaque($nome) {
		echo "<table width=\"700\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"status\">
					<tr>
						<td height=\"5\"></td>
					</tr>
					<tr>
						<td width=\"5\" height=\"5\" align=\"right\" valign=\"bottom\"><img src=\"imagens/curva-6-cima-esq.png\" alt=\"canto cima esquerdo\" width=\"5\" height=\"5\" /></td>
						<td height=\"5\" bgcolor=\"#EBEBEB\"></td>
						<td width=\"5\" height=\"5\" align=\"left\" valign=\"bottom\"><img src=\"imagens/curva-6-cima-dir.png\" alt=\"canto cima direito\" width=\"5\" height=\"5\" /></td>
					</tr>
					<tr>
						<td width=\"5\" bgcolor=\"#EBEBEB\"></td>
						<td bgcolor=\"#EBEBEB\" class=\"textoextragrande-preto\"><table height=\"24\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"pagina\">
							<tr>
								<td width=\"24\" height=\"24\" align=\"center\" valign=\"middle\"><img src=\"imagens/marcador-normal.png\" width=\"14\" height=\"14\" /></td>
								<td class=\"textoextragrande-preto\"><strong>$nome</strong></td>
							</tr>
						</table>
						</td>
						<td width=\"5\" bgcolor=\"#EBEBEB\"></td>
					</tr>
					<tr>
						<td width=\"5\" height=\"5\" align=\"right\" valign=\"top\"><img src=\"imagens/curva-6-baixo-esq.png\" alt=\"canto baixo esquerdo\" width=\"5\" height=\"5\" /></td>
						<td height=\"5\" bgcolor=\"#EBEBEB\"></td>
						<td width=\"5\" height=\"5\" align=\"left\" valign=\"top\"><img src=\"imagens/curva-6-baixo-dir.png\" alt=\"canto baixo direito\" width=\"5\" height=\"5\" /></td>
					</tr>
					<tr>
						<td height=\"5\"></td>
					</tr>
				</table>";
	}
	
	// cria botao interativo
	function botaointerativo($pm,$ps) {
		echo "<a href=\"?pm=$pm&ps=$ps\" onmouseout=\"MM_swapImgRestore()\" onmouseover=\"MM_swapImage('$ps','','imagens/$pm.$ps.in.png',1)\">
				<img src=\"imagens/$pm.$ps.out.png\" name=\"$ps\" width=\"243\" height=\"64\" border=\"0\" id=\"$ps\" /></a>";
	}
	
	// exibe mensagem de erro
	function exibeerro($msg) {
		echo "<table width=\"350\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"status\">
					<tr>
						<td width=\"5\" height=\"5\" align=\"right\" valign=\"bottom\"><img src=\"imagens/curva-6-cima-esq.png\" alt=\"canto cima esquerdo\" width=\"5\" height=\"5\" /></td>
						<td height=\"5\" bgcolor=\"#EBEBEB\"></td>
						<td width=\"5\" height=\"5\" align=\"left\" valign=\"bottom\"><img src=\"imagens/curva-6-cima-dir.png\" alt=\"canto cima direito\" width=\"5\" height=\"5\" /></td>
					</tr>
					<tr>
						<td width=\"5\" bgcolor=\"#EBEBEB\"></td>
						<td align=\"center\" bgcolor=\"#EBEBEB\"><table width=\"100%\" border=\"0\" cellspacing=\"10\" cellpadding=\"0\">
								<tr>
									<td width=\"48\" valign=\"top\"><img src=\"imagens/exclamacao.png\" alt=\"exclamacao\" width=\"48\" height=\"48\" /></td>
									<td align=\"center\" valign=\"middle\" class=\"textopequeno-preto\">$msg</td>
								</tr>
							</table></td>
						<td width=\"5\" bgcolor=\"#EBEBEB\"></td>
					</tr>
					<tr>
						<td width=\"5\" height=\"5\" align=\"right\" valign=\"top\"><img src=\"imagens/curva-6-baixo-esq.png\" alt=\"canto baixo esquerdo\" width=\"5\" height=\"5\" /></td>
						<td height=\"5\" bgcolor=\"#EBEBEB\"></td>
						<td width=\"5\" height=\"5\" align=\"left\" valign=\"top\"><img src=\"imagens/curva-6-baixo-dir.png\" alt=\"canto baixo direito\" width=\"5\" height=\"5\" /></td>
					</tr>
				</table>";
	}
?>