<?php 
	// index.php
	
	// carrega configura��o
	require("include.configuracao.php");
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?php echo $sistemanome; ?></title>
<link href="include.estilo.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" type="text/javascript" src="include.javascript.js"></script>
</head>
<body>
<?php 
	// verifica se � para mostrar o console, caso n�o
	if (!array_key_exists("carrega",$_GET)) {
?>
<script language="JavaScript" type="text/javascript">
	carregaconsole();
</script>
<br />
<br />
<br />
<br />
<table width="613" height="300" border="0" align="center" cellspacing="15" background="imagens/erro.gif">
	<tr>
		<td width="411" height="207" align="center" class="textopequeno-preto"><p><span class="textogrande-branco">Ops, temos um erro!</span><br />
				Seu navegador n&atilde;o est&aacute; configurado corretamente.<br />
				<br />
				<span class="textogrande-branco">Mozilla Firefox:</span><br />
				Voc&ecirc; tem que configurar digitando na barra de endere&ccedil;os &quot;about:config&quot;, localizar a chave &quot;dom.allow_scripts_to_close_windows&quot; e modific&aacute;-lo para &quot;true&quot; e dom.disable_open_during_load para &quot;false&quot;. Isso pode ser poss&iacute;vel dando um duplo-clique no item.</p>
			<p><span class="textogrande-branco">Internet Explorer e Opera Browser:</span><br />
		Desabilitar bloqueador pop-up!</p></td>
		<td width="202" align="center" valign="bottom" class="textomedio-branco"><a href="#" onClick="javascript:carregaconsole();"><strong><font color="#FFFFFF">ABRIR CONSOLE FOR&Ccedil;ADAMENTE</font></strong></a></td>
	</tr>
	<tr>
		<td colspan="2" align="center" class="textoextragrande-preto"><strong><?php echo "$sistemabreviatura $sistemaversao"; ?></strong></td>
	</tr>
</table>
<?php 
	// abre o console
	} elseif ($_GET["carrega"] == "console") {
?>
<table border="0" cellpadding="0" cellspacing="0" style="width:100%; height:100%;">
	<tr>
		<td align="center" valign="middle"><table width="750" height="490" border="0" align="center" cellpadding="0" cellspacing="10" id="main">
			<tr>
				<td width="730" height="72"><table width="730" height="72" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td><img src="imagens/logo-siget.png" alt="logotipo" width="201" height="56" hspace="5" /></td>
							<td width="72" align="center"><img src="imagens/logo-brasao.png" alt="brasao" width="57" height="64" /></td>
							<td width="173" align="center"><img src="imagens/logo-bairroescola.png" alt="bairroescola" width="158" height="72" /></td>
							<td width="168" align="center"><img src="imagens/logo-cic.png" alt="cic" width="153" height="59" vspace="0" /></td>
						</tr>
				</table></td>
			</tr>
			<tr>
				<td width="730" height="320"><iframe src="mainform.php" name="mainform" width="730" height="320" scrolling="Auto" frameborder="0" id="mainform"></iframe></td>
			</tr>
			<tr>
				<td width="730" height="58"><table width="730" height="58" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td><table width="100%" height="58" border="0" cellpadding="0" cellspacing="0" id="status">
									<tr>
										<td width="5" height="5" align="right" valign="bottom"><img src="imagens/curva-2-cima-esq.png" alt="canto cima esquerdo" width="5" height="5" /></td>
										<td height="5" bgcolor="#666666"></td>
										<td width="5" height="5" align="left" valign="bottom"><img src="imagens/curva-2-cima-dir.png" alt="canto cima direito" width="5" height="5" /></td>
									</tr>
									<tr>
										<td width="5" bgcolor="#666666"></td>
										<td align="center" bgcolor="#666666" class="textopequeno-branco"><?php echo "$sistemabreviatura $sistemaversao - $sistemanome"; ?><br>
											Prefeitura da Cidade de Nova Igua&ccedil;u<br>
											CIC - Centro para Inova&ccedil;&atilde;o e Competitividade</td>
										<td width="5" bgcolor="#666666"></td>
									</tr>
									<tr>
										<td width="5" height="5" align="right" valign="top"><img src="imagens/curva-2-baixo-esq.png" alt="canto baixo esquerdo" width="5" height="5" /></td>
										<td height="5" bgcolor="#666666"></td>
										<td width="5" height="5" align="left" valign="top"><img src="imagens/curva-2-baixo-dir.png" alt="canto baixo direito" width="5" height="5" /></td>
									</tr>
							</table></td>
							<td width="10">&nbsp;</td>
							<td width="66"><img src="imagens/logo-pcni.png" alt="pcni" name="pcni" width="66" height="58" id="pcni" /></td>
						</tr>
				</table></td>
			</tr>
		</table></td>
	</tr>
</table>
<?php
	}
?>
</body>
</html>
