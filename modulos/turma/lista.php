<?php topicodestaque("Turmas Cadastradas"); ?>
<?php
	$sql = mysql_query("SELECT COUNT(*) AS totalturma FROM turma WHERE codigo_telecentro='$codigotelecentro'");
	$totalturma = mysql_result($sql, 0, "totalturma");
	if ($totalturma > 0) {
?>
<table width="700" border="0" cellspacing="2" cellpadding="0">
	<tr>
		<td colspan="6" class="textomedio-preto">&nbsp;&nbsp;&nbsp;Foram encontrados <?php echo $totalturma; ?> turmas.<br />
			&nbsp;&nbsp;&nbsp;<span class="textopequeno-preto">As turmas em <font color="#FF0000">vermelhos</font> est�o concluidos ou expirados...</span></td>
	</tr>
	<tr>
		<td colspan="2" align="center" bgcolor="#FFCCCC" class="linha-fundo">C&oacute;digo</td>
		<td align="center" bgcolor="#FFCCCC" class="linha-fundo">Turma</td>
		<td width="130" align="center" bgcolor="#FFCCCC" class="linha-fundo">Data / Hora Inicial </td>
		<td width="130" align="center" bgcolor="#FFCCCC" class="linha-fundo">Data / Hora Termino </td>
		<td width="25" align="center" bgcolor="#FFCCCC" class="linha-fundodireito">N&ordm;</td>
	</tr>
	<?php 
		$sql_1 = mysql_query("SELECT * FROM turma WHERE codigo_telecentro='$codigotelecentro' ORDER BY id DESC");
		while ($turma = mysql_fetch_array($sql_1)) {
			$codigo_telecentro = $turma["codigo_telecentro"];
			$id_turma = $turma["id"];
			$id_curso = $turma["id_curso"];
			$id_codigoturma = $turma["id_codigoturma"];
			$datainicio = conversordata($turma["datainicio"], "/", "mysql.normal");
			$datatermino = conversordata($turma["datatermino"], "/", "mysql.normal");
			$codigo_turma = $turma["codigo"];
			
			// zera variaveis
			$cursofinalizado = "";
			$diasemana = "";
			$codigoSemana = "";
			$diasemana_tmp = "";
			
			// faz os dias das aulas por extenso e por cod
			$sql_2 = mysql_query("SELECT COUNT(*) AS totalturma_grade FROM turma_grade WHERE codigo_turma='$codigo_turma'");
			// conta quantas grades tem com essa turma
			$totalturma_grade = mysql_result($sql_2, 0, "totalturma_grade");
			// se existir somente uma grade
			if ($totalturma_grade == 1) {
				$sql_3 = mysql_query("SELECT * FROM turma_grade WHERE codigo_turma='$codigo_turma' ORDER BY diasemana ASC");
				if ($turma_grade = mysql_fetch_array($sql_3)){
					// imprime o dia da semana em extenso
					$diasemana = " - ". ucfirst(conversorextenso($turma_grade["diasemana"], "num.semana"));
					// imprime o dia da semana resumido
					if ($turma_grade["diasemana"] == "1") {
						$codigoSemana = "D";
					} elseif ($turma_grade["diasemana"] == "7") {
						$codigoSemana = "S";
					} else {
						$codigoSemana = $turma_grade["diasemana"];
					}
				}
			} elseif ($totalturma_grade > 1) { // se existir mais de uma grade
				// primeiro dia
				$sql_3 = mysql_query("SELECT * FROM turma_grade WHERE codigo_turma='$codigo_turma' ORDER BY diasemana ASC");
				$primeiroDia = mysql_result($sql_3, 0, "diasemana");
				while ($turma_grade = mysql_fetch_array($sql_3)) {
					$diasemana_tmp = $diasemana_tmp . ", " . ucfirst(conversorextenso($turma_grade["diasemana"], "num.semana")); // imprime o dia da semana em extenso
					$codigoSemana = $primeiroDia . "-" . $turma_grade["diasemana"]; // imprime o dia da semana em numeral
					$diasemana = " - ". ucfirst(conversorextenso($primeiroDia, "num.semana")) . $diasemana_tmp; 
				}
			} else {
				$diasemana = "";
			}
			
			// conta quantos usu�rios tem cadastrado
			$sql_4 = mysql_query("SELECT COUNT(*) AS totalturma_usuario FROM turma_usuario WHERE codigo_turma='$codigo_turma'");
			$totalturma_usuario = mysql_result($sql_4, 0, "totalturma_usuario"); // conta quantas grades tem com esse diariodeclasse
	
			// hora inicial da turma
			$sql_5 = mysql_query("SELECT * FROM turma_grade WHERE codigo_turma='$codigo_turma' ORDER BY diasemana ASC");
			if ($turma_grade = mysql_fetch_array($sql_5)) {
				$horainicio = mysql_result($sql_5, 0, "horainicio");
				$horafim = mysql_result($sql_5, 0, "horafim");
			} else {
				$horainicio = "00:00";
				$horafim = "00:00";
			}
			
			// nome da turma
			$sql_6 = mysql_query("SELECT * FROM codigoturma WHERE id=$id_codigoturma");
			$codigo = mysql_result($sql_6, 0, "codigo");
			$nome = mysql_result($sql_6, 0, "nome");
			
			// verifica se a turma j� expirou
			$dtaini = conversordata($turma["datainicio"], "", "mysql.mysql");
			$dtaend = conversordata($turma["datatermino"], "", "mysql.mysql");
			$data = date("Ymd");
			if ($dtaini >= $data || $dtaend <= $data) {
				$cursofinalizado = " style=\"color:#FF0000\"";
			}
?>
	<tr onmouseover="this.className='celulaefeito-over'" onmouseout="this.className='celulaefeito-out'" onclick="javascript:carregapagina('?pm=turma&amp;ps=edita&id=<?php echo $id_turma; ?>','_self')" style="cursor:pointer">
		<td width="43" align="right" class="linha-fundo"<?php echo $cursofinalizado; ?>><?php echo $codigo; ?><?php echo $codigoSemana; ?></td>
		<td width="55" class="linha-fundo"<?php echo $cursofinalizado; ?>><?php echo $horainicio; ?> h</td>
		<td class="linha-fundo"<?php echo $cursofinalizado; ?>><?php echo $nome; ?><?php echo $diasemana; ?></td>
		<td width="130" align="center" class="linha-fundo"<?php echo $cursofinalizado; ?>><?php echo $datainicio; ?> - <?php echo $horainicio; ?></td>
		<td width="130" align="center" class="linha-fundo"<?php echo $cursofinalizado; ?>><?php echo $datatermino; ?> - <?php echo $horafim; ?></td>
		<td width="25" align="center" class="linha-fundodireito"<?php echo $cursofinalizado; ?>><?php echo $totalturma_usuario; ?></td>
	</tr>
	<?php 
		}
?>
</table>
<?php 
	} else {
?>
<br />
<table width="350" border="0" cellpadding="0" cellspacing="0" id="status">
	<tr>
		<td width="5" height="5" align="right" valign="bottom"><img src="imagens/curva-6-cima-esq.png" alt="canto cima esquerdo" width="5" height="5" /></td>
		<td height="5" bgcolor="#EBEBEB"></td>
		<td width="5" height="5" align="left" valign="bottom"><img src="imagens/curva-6-cima-dir.png" alt="canto cima direito" width="5" height="5" /></td>
	</tr>
	<tr>
		<td width="5" bgcolor="#EBEBEB"></td>
		<td align="center" bgcolor="#EBEBEB"><table width="100%" border="0" cellspacing="10" cellpadding="0">
				<tr>
					<td width="48" valign="top"><img src="imagens/exclamacao.png" alt="exclamacao" width="48" height="48" /></td>
					<td align="center" valign="middle" class="textopequeno-preto">Nenhuma turma encontrado! </td>
				</tr>
			</table></td>
		<td width="5" bgcolor="#EBEBEB"></td>
	</tr>
	<tr>
		<td width="5" height="5" align="right" valign="top"><img src="imagens/curva-6-baixo-esq.png" alt="canto baixo esquerdo" width="5" height="5" /></td>
		<td height="5" bgcolor="#EBEBEB"></td>
		<td width="5" height="5" align="left" valign="top"><img src="imagens/curva-6-baixo-dir.png" alt="canto baixo direito" width="5" height="5" /></td>
	</tr>
</table>
<?php 
	}
?>
<br>
<table width="450" border="0" cellspacing="3" cellpadding="0">
	<tr>
		<td width="100%"><input type="button" class="button-normal" value="Cancelar" onClick="javascript:carregapagina('?pm=turma','_self')" /></td>
	</tr>
</table>
