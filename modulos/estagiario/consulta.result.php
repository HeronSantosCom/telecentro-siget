<?php topicodestaque("Resultado da consulta"); ?>
<?php 
	// pega a variavel busca do formulario de consulta
	$stringBusca = trim($_POST["busca"]);
	
	// monta a string da conexao
	$sqlforma = "WHERE matricula = '".$stringBusca."' OR nome LIKE '%".$stringBusca."%' ORDER BY nome ASC";
	
	// conta quantos estagiários existem
	$sql = mysql_query("SELECT COUNT(*) AS totalestagiario FROM estagiario ". $sqlforma);
	$totalestagiario = mysql_result($sql, 0, "totalestagiario");

	// se existir mais que um
	if ($totalestagiario > 0) {
?>
<table width="700" border="0" cellspacing="2" cellpadding="0">
	<tr>
		<td colspan="3" class="textomedio-preto">&nbsp;&nbsp;&nbsp;Foram encontrados <?php echo $totalestagiario; ?> estagi&aacute;rios .<br />
			&nbsp;&nbsp;&nbsp;<span class="textopequeno-preto">Os estagi&aacute;rios em <font color="#FF0000">vermelhos</font> tem seus dados incompletos... Preencha-os por favor!</span></td>
	</tr>
	<tr>
		<td width="100" align="center" bgcolor="#FFCCCC" class="linha-fundo">Matr&iacute;cula</td>
		<td align="center" bgcolor="#FFCCCC" class="linha-fundo">Nome</td>
		<td width="100" align="center" bgcolor="#FFCCCC" class="linha-fundodireito">Admissão</td>
	</tr>
	<?php 
		// lista os estagiários
		$sql = mysql_query("SELECT * FROM estagiario " . $sqlforma);
		while($estagiario = mysql_fetch_array($sql)) {
			// verifica se existe algum dado incompleto
			if ($estagiario["identidadenumero"] == "0" || $estagiario["ctpsnumero"] == "0" || $estagiario["ctpsserie"] == "0" || $estagiario["cpf"] == "0" || $estagiario["tituloeleitornumero"] == "0" || $estagiario["tituloeleitorsecao"] == "0") {
				// pinta em vermelho
				$estagiarioIncompleto = "#FF0000";
			}
?>
	<tr onmouseover="this.className='celulaefeito-over'" onmouseout="this.className='celulaefeito-out'" onclick="javascript:carregapagina('?pm=estagiario&amp;ps=edita&id=<?php echo $estagiario["id"]; ?>','_self')" style="cursor:pointer">
		<td align="center" class="linha-fundo" style="color:<?php echo $estagiarioIncompleto ?>"><?php echo $estagiario["matricula"]; ?></td>
		<td class="linha-fundo" style="color:<?php echo $estagiarioIncompleto ?>"><?php echo $estagiario["nome"]; ?></td>
		<td align="center" class="linha-fundodireito" style="color:<?php echo $estagiarioIncompleto ?>"><?php echo conversordata($estagiario["dataadmissao"], "/", "mysql.normal"); ?></td>
	</tr>
	<?php 
		}
?>
</table>
<?php 
	} else {
?>
<br />
<table width="350" border="0" cellpadding="0" cellspacing="0" id="status">
	<tr>
		<td width="5" height="5" align="right" valign="bottom"><img src="imagens/curva-6-cima-esq.png" alt="canto cima esquerdo" width="5" height="5" /></td>
		<td height="5" bgcolor="#EBEBEB"></td>
		<td width="5" height="5" align="left" valign="bottom"><img src="imagens/curva-6-cima-dir.png" alt="canto cima direito" width="5" height="5" /></td>
	</tr>
	<tr>
		<td width="5" bgcolor="#EBEBEB"></td>
		<td align="center" bgcolor="#EBEBEB"><table width="100%" border="0" cellspacing="10" cellpadding="0">
				<tr>
					<td width="48" valign="top"><img src="imagens/exclamacao.png" alt="exclamacao" width="48" height="48" /></td>
					<td align="center" valign="middle" class="textopequeno-preto">Estagi&aacute;rio n&atilde;o encontrado!<br />
						<br />
						Em caso de d&uacute;vidas clique em &quot;?&quot;!</td>
				</tr>
			</table></td>
		<td width="5" bgcolor="#EBEBEB"></td>
	</tr>
	<tr>
		<td width="5" height="5" align="right" valign="top"><img src="imagens/curva-6-baixo-esq.png" alt="canto baixo esquerdo" width="5" height="5" /></td>
		<td height="5" bgcolor="#EBEBEB"></td>
		<td width="5" height="5" align="left" valign="top"><img src="imagens/curva-6-baixo-dir.png" alt="canto baixo direito" width="5" height="5" /></td>
	</tr>
</table>
<?php 
	}
?>
<br />
<table width="450" border="0" cellspacing="3" cellpadding="0">
	<tr>
		<td><input type="button" class="button-normal" value="Realizar uma nova consulta" onclick="javascript:carregapagina('?pm=estagiario&amp;ps=consulta','_self')" /></td>
	</tr>
</table>