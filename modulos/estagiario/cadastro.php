<form action="?pm=estagiario&amp;ps=cadastro.post" method="post" name="estagiario" id="estagiario" onSubmit="return validaEstagiario();">
	<?php topicodestaque("Identifica&ccedil;&atilde;o da pessoa"); ?>
	<table width="700" border="0" cellspacing="2" cellpadding="0">
		<tr>
			<td width="200" class="linha-fundo">Nome:</td>
			<td><input name="nome" type="text" class="input-destacado" id="nome" maxlength="150" style="width:100%" onKeyUp="this.value = this.value.toUpperCase();" /></td>
		</tr>
		<tr>
			<td width="200" class="linha-fundo">Data de nascimento: </td>
			<td><table width="30%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><input name="datanascimento" type="text" class="input-destacado" id="datanascimento" maxlength="10" style="width:100%" onKeyUp="formataCampo(this, '##/##/####', event);" onKeyPress="return bloqueiaAlfa(event);" /></td>
						<td width="2">&nbsp;</td>
						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...')" value="?" /></td>
					</tr>
				</table></td>
		</tr>
		<tr>
			<td width="200" class="linha-fundo">Sexo:</td>
			<td><select name="id_sexo" id="id_sexo" class="input-destacado" style="width:50%" >
					<option value="" selected="selected">Selecione...</option>
					<option value="">-------------------</option>
					<?php 
		// enfilera os sexos
		$sql = mysql_query("select * from sexo order by sexo asc");
		while ($sexo = mysql_fetch_array($sql)) {
			echo "<option value=\"".$sexo['id']."\">".$sexo['sexo']."</option>";
		}
?>
				</select></td>
		</tr>
		<tr>
			<td width="200" class="linha-fundo">Nome completo do pai:</td>
			<td><input name="nomepai" type="text" class="input-normal" id="nomepai" maxlength="150" style="width:100%" onKeyUp="this.value = this.value.toUpperCase();"/></td>
		</tr>
		<tr>
			<td width="200" class="linha-fundo">Nome completo da m&atilde;e:</td>
			<td><input name="nomemae" type="text" class="input-normal" id="nomemae" maxlength="150" style="width:100%" onKeyUp="this.value = this.value.toUpperCase();"/></td>
		</tr>
		<tr>
			<td class="linha-fundo">Matr&iacute;cula:</td>
			<td><table width="30%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><input name="matricula" type="text" class="input-destacado" id="matricula" maxlength="12" style="width:100%" onKeyUp="formataCampo(this, '##/###.###-#', event);" onKeyPress="return bloqueiaAlfa(event);" /></td>
						<td width="2">&nbsp;</td>
						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...')" value="?" /></td>
					</tr>
				</table></td>
		</tr>
		<tr>
			<td class="linha-fundo">Data de admiss&atilde;o:</td>
			<td><table width="30%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><input name="dataadmissao" type="text" class="input-destacado" id="dataadmissao" maxlength="10" style="width:100%" onKeyUp="formataCampo(this, '##/##/####', event);" onKeyPress="return bloqueiaAlfa(event);" /></td>
						<td width="2">&nbsp;</td>
						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...')" value="?" /></td>
					</tr>
				</table></td>
		</tr>
		<tr>
			<td class="linha-fundo">Hora de entrada:</td>
			<td><table width="30%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><input name="horaentrada" type="text" class="input-destacado" id="horaentrada" maxlength="5" style="width:100%" onKeyUp="formataCampo(this, '##:##', event);" onKeyPress="return bloqueiaAlfa(event);" /></td>
						<td width="2">&nbsp;</td>
						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...')" value="?" /></td>
					</tr>
				</table></td>
		</tr>
		<tr>
			<td class="linha-fundo">Hora de sa&iacute;da:</td>
			<td><table width="30%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><input name="horasaida" type="text" class="input-destacado" id="horasaida" maxlength="5" style="width:100%" onKeyUp="formataCampo(this, '##:##', event);" onKeyPress="return bloqueiaAlfa(event);" /></td>
						<td width="2">&nbsp;</td>
						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...')" value="?" /></td>
					</tr>
				</table></td>
		</tr>
		<tr>
			<td class="linha-fundo">Unidade atuante:</td>
			<td><select name="id_telecentro" id="id_telecentro" class="input-destacado" style="width:100%" >
					<option value="" selected="selected">Selecione...</option>
					<option value="">-------------------</option>
					<?php 
		// enfilera os graus escolares
		$sql = mysql_query("select * from telecentro order by nome asc");
		while ($telecentro = mysql_fetch_array($sql)) {
			echo "<option value=\"".$telecentro['codigo']."\">".$telecentro['nome']."</option>";
		}
?>
				</select></td>
		</tr>
		<tr>
			<td width="200" class="linha-fundo">Estado civil:</td>
			<td><select name="id_estadocivil" id="id_estadocivil" class="input-destacado" style="width:50%" >
					<option value="" selected="selected">Selecione...</option>
					<option value="">-------------------</option>
					<?php 
		// enfilera os estados civis
		$sql = mysql_query("select * from estadocivil order by estadocivil asc");
		while ($estadocivil = mysql_fetch_array($sql)) {
			echo "<option value=\"".$estadocivil['id']."\">".$estadocivil['estadocivil']."</option>";
		}
?>
				</select></td>
		</tr>
	</table>
	<?php topicodestaque("Identificação do domicílio"); ?>
	<table width="700" border="0" cellspacing="2" cellpadding="0">
		<tr>
			<td width="200" align="left" class="linha-fundo"> Logradouro:</td>
			<td><input name="enderecologradouro" type="text" class="input-destacado" id="enderecologradouro" maxlength="100" style="width:100%" onKeyUp="this.value = this.value.toUpperCase();"/></td>
		</tr>
		<tr>
			<td width="200" align="left" class="linha-fundo">N&uacute;mero:</td>
			<td><table width="30%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><input name="endereconumero" type="text" class="input-destacado" id="endereconumero" style="width:100%"onkeypress="return bloqueiaAlfa(event);" maxlength="7" /></td>
						<td width="2">&nbsp;</td>
						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...')" value="?" /></td>
					</tr>
				</table></td>
		</tr>
		<tr>
			<td width="200" align="left" class="linha-fundo">Complemento:</td>
			<td><input name="enderecocomplemento" type="text" class="input-normal" id="enderecocomplemento" maxlength="50" style="width:100%" onKeyUp="this.value = this.value.toUpperCase();"/></td>
		</tr>
		<tr>
			<td width="200" align="left" class="linha-fundo">CEP:</td>
			<td><table width="30%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><input name="enderecocep" type="text" class="input-normal" id="enderecocep" style="width:100%" onKeyUp="formataCampo(this, '#####-###', event);" onKeyPress="return bloqueiaAlfa(event);" maxlength="9" /></td>
						<td width="2">&nbsp;</td>
						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...')" value="?" /></td>
					</tr>
				</table></td>
		</tr>
		<tr>
			<td width="200" align="left" class="linha-fundo">Bairro:</td>
			<td><select name="endereco_id_bairro" id="endereco_id_bairro" class="input-destacado" style="width:75%" >
					<option value="" selected="selected">Selecione...</option>
					<option value="">-------------------</option>
					<?php 
		// enfilera bairros
		$sql = mysql_query("select * from bairro order by bairro asc");
		while ($bairro = mysql_fetch_array($sql)) {
			echo "<option value=\"".$bairro['id']."\">".$bairro['bairro']."</option>";
		}
?>
				</select></td>
		</tr>
		<tr>
			<td width="200" align="left" class="linha-fundo">Munic&iacute;pio:</td>
			<td><select name="endereco_id_municipio" id="endereco_id_municipio" class="input-destacado" style="width:75%" >
					<option value="" selected="selected">Selecione...</option>
					<option value="">-------------------</option>
					<?php 
		// enfilera municipios
		$sql = mysql_query("select * from municipio order by municipio asc");
		while ($municipio = mysql_fetch_array($sql)) {
			echo "<option value=\"".$municipio['id']."\">".$municipio['municipio']."</option>";
		}
?>
				</select></td>
		</tr>
		<tr>
			<td width="200" align="left" class="linha-fundo">Estado:</td>
			<td><select name="endereco_id_uf" id="endereco_id_uf" class="input-destacado" style="width:50%" >
					<option value="" selected="selected">Selecione...</option>
					<option value="">-------------------</option>
					<?php 
		// enfilera os estados
		$sql = mysql_query("select * from uf order by estado asc");
		while ($uf = mysql_fetch_array($sql)) {
			echo "<option value=\"".$uf['id']."\">".$uf['estado']."</option>";
		}
?>
				</select></td>
		</tr>
	</table>
	<?php topicodestaque("Formas de contato"); ?>
	<table width="700" border="0" cellspacing="2" cellpadding="0">
		<tr>
			<td width="200" align="left" class="linha-fundo">Telefone Fixo:</td>
			<td><table width="30%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><input name="telefonefixo" type="text" class="input-destacado" id="telefonefixo" style="width:100%" onKeyUp="formataCampo(this, '## ####-####', event);" onKeyPress="return bloqueiaAlfa(event);" maxlength="12"/></td>
						<td width="2">&nbsp;</td>
						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...')" value="?" /></td>
					</tr>
				</table></td>
		</tr>
		<tr>
			<td width="200" align="left" class="linha-fundo">Telefone Celular:</td>
			<td><table width="30%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><input name="telefonecelular" type="text" class="input-normal" id="telefonecelular" style="width:100%" onKeyUp="formataCampo(this, '## ####-####', event);" onKeyPress="return bloqueiaAlfa(event);" maxlength="12"/></td>
						<td width="2">&nbsp;</td>
						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...')" value="?" /></td>
					</tr>
				</table></td>
		</tr>
		<tr>
			<td width="200" align="left" class="linha-fundo">E-mail:</td>
			<td><table width="75%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><input name="email" type="text" class="input-normal" id="email" style="width:100%" onKeyUp="this.value = this.value.toLowerCase();" maxlength="100"/></td>
						<td width="2">&nbsp;</td>
						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...')" value="?" /></td>
					</tr>
				</table></td>
		</tr>
	</table>
	<?php topicodestaque("Documentos"); ?>
	<table width="700" border="0" cellspacing="2" cellpadding="0">
		<tr>
			<td width="200" align="left" class="linha-fundo">NIS:</td>
			<td><table width="35%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><input name="nis" type="text" class="input-normal" id="nis" style="width:100%" maxlength="11" onKeyPress="return bloqueiaAlfa(event);" /></td>
						<td width="2">&nbsp;</td>
						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...')" value="?" /></td>
					</tr>
				</table></td>
		</tr>
		<tr>
			<td width="200" align="left" class="linha-fundo">CPF:</td>
			<td><table width="35%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><input name="cpf" type="text" class="input-destacado" id="cpf" style="width:100%" onKeyUp="formataCampo(this, '###.###.###-##', event);" onKeyPress="return bloqueiaAlfa(event);" maxlength="14"/></td>
						<td width="2">&nbsp;</td>
						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...\n\nCaso n&atilde;o tenha em m&atilde;os, digite somente *!')" value="?" /></td>
					</tr>
				</table></td>
		</tr>
		<tr>
			<td width="200" class="linha-fundo">Identidade:</td>
			<td><table width="30%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><input name="identidadenumero" type="text" class="input-destacado" id="identidadenumero" style="width:100%" maxlength="11"onkeypress="return bloqueiaAlfa(event);" /></td>
						<td width="2">&nbsp;</td>
						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...\n\nCaso n&atilde;o tenha em m&atilde;os, digite somente *!')" value="?" /></td>
					</tr>
				</table></td>
		</tr>
		<tr>
			<td width="200" class="linha-fundo"><img src="imagens/marcador-normal.png" alt="marcador" width="14" height="14" hspace="2" vspace="0" border="0" align="absmiddle" />Data de emiss&atilde;o:</td>
			<td><table width="30%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><input name="identidadedataemissao" type="text" class="input-normal" id="identidadedataemissao" style="width:100%" onKeyUp="formataCampo(this, '##/##/####', event);" onKeyPress="return bloqueiaAlfa(event);" maxlength="10"/></td>
						<td width="2">&nbsp;</td>
						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...')" value="?" /></td>
					</tr>
				</table></td>
		</tr>
		<tr>
			<td width="200" class="linha-fundo"><img src="imagens/marcador-normal.png" alt="marcador" width="14" height="14" hspace="2" vspace="0" border="0" align="absmiddle" /><span class="celulaLinha">Org&atilde;o emissor</span>:</td>
			<td><table width="30%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><input name="identidadeorgaoemissor" type="text" class="input-normal" id="identidadeorgaoemissor" style="width:100%" onKeyUp="this.value = this.value.toUpperCase();" maxlength="10"/></td>
						<td width="2">&nbsp;</td>
						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...')" value="?" /></td>
					</tr>
				</table></td>
		</tr>
		<tr>
			<td width="200" class="linha-fundo">CTPS:</td>
			<td><table width="25%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><input name="ctpsnumero" type="text" class="input-destacado" id="ctpsnumero" style="width:100%"maxlength="7" onKeyPress="return bloqueiaAlfa(event);"/></td>
						<td width="2">&nbsp;</td>
						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...\n\nCaso n&atilde;o tenha em m&atilde;os, digite somente *!')" value="?" /></td>
					</tr>
				</table></td>
		</tr>
		<tr>
			<td width="200" class="linha-fundo"><img src="imagens/marcador-normal.png" alt="marcador" width="14" height="14" hspace="2" vspace="0" border="0" align="absmiddle" />S&eacute;rie:</td>
			<td><table width="15%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><input name="ctpsserie" type="text" class="input-destacado" id="ctpsserie" style="width:100%"maxlength="5" onKeyPress="return bloqueiaAlfa(event);"/></td>
						<td width="2">&nbsp;</td>
						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...\n\nCaso n&atilde;o tenha em m&atilde;os, digite somente *!')" value="?" /></td>
					</tr>
				</table></td>
		</tr>
		<tr>
			<td width="200" class="linha-fundo"><img src="imagens/marcador-normal.png" alt="marcador" width="14" height="14" hspace="2" vspace="0" border="0" align="absmiddle" />Data de emiss&atilde;o:</td>
			<td><table width="30%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><input name="ctpsdataemissao" type="text" class="input-normal" id="ctpsdataemissao" style="width:100%" onKeyUp="formataCampo(this, '##/##/####', event);" onKeyPress="return bloqueiaAlfa(event);" maxlength="10"/></td>
						<td width="2">&nbsp;</td>
						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...')" value="?" /></td>
					</tr>
				</table></td>
		</tr>
		<tr>
			<td width="200" class="linha-fundo"><img src="imagens/marcador-normal.png" alt="marcador" width="14" height="14" hspace="2" vspace="0" border="0" align="absmiddle" />Estado:</td>
			<td><select name="ctps_id_uf" id="ctps_id_uf" style="width:50%" class="input-normal" >
					<option value="" selected="selected">Selecione...</option>
					<option value="">-------------------</option>
					<?php 
		// enfilera os uf's da ctps
		$sql = mysql_query("select * from uf order by estado asc");
		while ($uf = mysql_fetch_array($sql)) {
			echo "<option value=\"".$uf['id']."\">".$uf['estado']."</option>";
		}
?>
				</select></td>
		</tr>
		<tr>
			<td width="200" class="linha-fundo">TItulo Eleitor: </td>
			<td><table width="35%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><input name="tituloeleitornumero" type="text" class="input-destacado" id="tituloeleitornumero" style="width:100%"maxlength="13" onKeyPress="return bloqueiaAlfa(event);"/></td>
						<td width="2">&nbsp;</td>
						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...\n\nCaso n&atilde;o tenha em m&atilde;os, digite somente *!')" value="?" /></td>
					</tr>
				</table></td>
		</tr>
		<tr>
			<td width="200" class="linha-fundo"><img src="imagens/marcador-normal.png" alt="marcador" width="14" height="14" hspace="2" vspace="0" border="0" align="absmiddle" />Zona:</td>
			<td><table width="15%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><input name="tituloeleitorzona" type="text" class="input-destacado" id="tituloeleitorzona" style="width:100%"maxlength="4" onKeyPress="return bloqueiaAlfa(event);"/></td>
						<td width="2">&nbsp;</td>
						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...\n\nCaso n&atilde;o tenha em m&atilde;os, digite somente *!')" value="?" /></td>
					</tr>
				</table></td>
		</tr>
		<tr>
			<td width="200" class="linha-fundo"><img src="imagens/marcador-normal.png" alt="marcador" width="14" height="14" hspace="2" vspace="0" border="0" align="absmiddle" />Se&ccedil;&atilde;o:</td>
			<td><table width="15%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><input name="tituloeleitorsecao" type="text" class="input-destacado" id="tituloeleitorsecao" style="width:100%" maxlength="4" onKeyPress="return bloqueiaAlfa(event);"/></td>
						<td width="2">&nbsp;</td>
						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...\n\nCaso n&atilde;o tenha em m&atilde;os, digite somente *!')" value="?" /></td>
					</tr>
				</table></td>
		</tr>
	</table>
	<?php topicodestaque("Qualifica&ccedil;&atilde;o escolar"); ?>
	<table width="700" border="0" cellspacing="2" cellpadding="0">
		<tr>
			<td width="200" class="linha-fundo">Frequenta institui&ccedil;&atilde;o:</td>
			<td><select name="escola_id_escolatipo" id="escola_id_escolatipo" class="input-destacado" style="width:50%" >
					<option value="" selected="selected">Selecione...</option>
					<option value="">-------------------</option>
					<?php 
		// enfilera tipo de escola
		$sql = mysql_query("select * from escolatipo order by tipo asc");
		while ($escolatipo = mysql_fetch_array($sql)) {
			echo "<option value=\"".$escolatipo['id']."\">".$escolatipo['tipo']."</option>";
		}
?>
				</select></td>
		</tr>
		<tr>
			<td width="200" class="linha-fundo">Grau de instru&ccedil;&atilde;o:</td>
			<td><select name="escola_id_escolagrau" id="escola_id_escolagrau" class="input-destacado" style="width:100%" >
					<option value="" selected="selected">Selecione...</option>
					<option value="">-------------------</option>
					<?php 
		// enfilera os graus escolares
		$sql = mysql_query("select * from escolagrau order by grau asc");
		while ($escolagrau = mysql_fetch_array($sql)) {
			echo "<option value=\"".$escolagrau['id']."\">".$escolagrau['grau']."</option>";
		}
?>
				</select></td>
		</tr>
		<tr>
			<td width="200" class="linha-fundo">S&eacute;rie escolar:</td>
			<td><select name="escola_id_escolaserie" id="escola_id_escolaserie" class="input-destacado" style="width:100%" >
					<option value="" selected="selected">Selecione...</option>
					<option value="">-------------------</option>
					<?php 
		// enfilera as s&eacute;ries escolares
		$sql = mysql_query("select * from escolaserie order by serie asc");
		while ($escolaserie = mysql_fetch_array($sql)) {
			echo "<option value=\"".$escolaserie['id']."\">".$escolaserie['serie']."</option>";
		}
?>
				</select></td>
		</tr>
		<tr>
			<td width="200" class="linha-fundo">Turno:</td>
			<td><select name="escola_id_escolaturno" id="escola_id_escolaturno" class="input-destacado" style="width:50%" >
					<option value="" selected="selected">Selecione...</option>
					<option value="">-------------------</option>
					<?php 
		// enfilera turnos
		$sql = mysql_query("select * from escolaturno order by turno asc");
		while ($escolaturno = mysql_fetch_array($sql)) {
			echo "<option value=\"".$escolaturno['id']."\">".$escolaturno['turno']."</option>";
		}
?>
				</select></td>
		</tr>
		<tr>
			<td width="200" class="linha-fundo">Nome da institui&ccedil;&atilde;o:</td>
			<td><input name="escola_escolanome" type="text" class="input-destacado" id="escola_escolanome" maxlength="150" style="width:100%" onKeyUp="this.value = this.value.toUpperCase();"/></td>
		</tr>
	</table>
	<br />
	<table width="450" height="25" border="0" cellspacing="2" cellpadding="0">
		<tr>
			<td align="center" bgcolor="#FFCCCC" class="linha-fundodireito">Todos os campos em <strong>rosa beb&ecirc;</strong>  s&atilde;o obrigat&oacute;rios!</td>
		</tr>
	</table>
	<br />
	<table width="700" border="0" cellspacing="3" cellpadding="0">
		<tr>
			<td width="35%"><input type="button" class="button-normal" value="Cancelar" onClick="javascript:carregapagina('?pm=estagiario','_self')" /></td>
			<td width="65%"><input name="post" type="submit" class="button-destacado" id="post" value="Cadastrar" /></td>
		</tr>
	</table>
</form>
