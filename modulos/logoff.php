<br />
<?php 
	if(empty($_GET["acao"])) {
?>
<table width="350" border="0" cellpadding="0" cellspacing="0" id="status">
	<tr>
		<td width="5" height="5" align="right" valign="bottom"><img src="imagens/curva-6-cima-esq.png" alt="canto cima esquerdo" width="5" height="5" /></td>
		<td height="5" bgcolor="#EBEBEB"></td>
		<td width="5" height="5" align="left" valign="bottom"><img src="imagens/curva-6-cima-dir.png" alt="canto cima direito" width="5" height="5" /></td>
	</tr>
	<tr>
		<td width="5" bgcolor="#EBEBEB"></td>
		<td align="center" bgcolor="#EBEBEB"><table width="100%" border="0" cellspacing="10" cellpadding="0">
				<tr>
					<td width="48" height="48"><img src="imagens/logoff.png" alt="logoff" width="48" height="48" /></td>
					<td align="center" class="textogrande-preto"><strong class="textoextragrande-preto">Encerrar Sess&atilde;o </strong></td>
				</tr>
				<tr>
					<td valign="top">&nbsp;</td>
					<td align="center" valign="top" class="textomedio-preto">Deseja realmente encerrar sess&atilde;o?<br />
						<br />
						<table width="100%" border="0" cellspacing="3" cellpadding="0">
							<tr>
								<td width="35%"><input name="acaoNao" type="button" class="button-normal" id="acaoNao" value="N&atilde;o" onclick="javascript:carregapagina('javascript:history.go(-1)','_self')" /></td>
								<td width="65%"><input name="acaoSim" type="submit" class="button-destacado" id="acaoSim" value="Sim" onclick="javascript:carregapagina('?pm=logoff&acao=sair','_self')" /></td>
							</tr>
						</table></td>
				</tr>
			</table></td>
		<td width="5" bgcolor="#EBEBEB"></td>
	</tr>
	<tr>
		<td width="5" height="5" align="right" valign="top"><img src="imagens/curva-6-baixo-esq.png" alt="canto baixo esquerdo" width="5" height="5" /></td>
		<td height="5" bgcolor="#EBEBEB"></td>
		<td width="5" height="5" align="left" valign="top"><img src="imagens/curva-6-baixo-dir.png" alt="canto baixo direito" width="5" height="5" /></td>
	</tr>
</table>
<?php 
	} elseif ($_GET["acao"] == "sair") {
		unset($_SESSION["id_acessonivel"]);
		unset($_SESSION["usuario"]);
		unset($_SESSION["senha"]);
?>
<meta http-equiv="refresh" content="1;URL=javascript:top.close()" />
<table width="350" border="0" cellpadding="0" cellspacing="0" id="status">
	<tr>
		<td width="5" height="5" align="right" valign="bottom"><img src="imagens/curva-6-cima-esq.png" alt="canto cima esquerdo" width="5" height="5" /></td>
		<td height="5" bgcolor="#EBEBEB"></td>
		<td width="5" height="5" align="left" valign="bottom"><img src="imagens/curva-6-cima-dir.png" alt="canto cima direito" width="5" height="5" /></td>
	</tr>
	<tr>
		<td width="5" bgcolor="#EBEBEB"></td>
		<td align="center" bgcolor="#EBEBEB"><table width="100%" border="0" cellspacing="10" cellpadding="0">
				<tr>
					<td width="48" height="48"><img src="imagens/concluido.png" alt="login" width="48" height="48" /></td>
					<td align="center" class="textogrande-preto"><strong class="textoextragrande-preto">Encerrar Sess&atilde;o</strong></td>
				</tr>
				<tr>
					<td valign="top">&nbsp;</td>
					<td align="center" valign="top" class="textomedio-preto">Sess&atilde;o encerrada com sucesso!<br />
						<br />
						Caso esta janela n&atilde;o feche automaticamente, clique no bot&atilde;o abaixo... <br />
						<br />
						<input name="acaoFechar" type="submit" class="button-destacado" id="acaoFechar" value="Fechar janela" onclick="javascript:top.close()" /></td>
				</tr>
			</table></td>
		<td width="5" bgcolor="#EBEBEB"></td>
	</tr>
	<tr>
		<td width="5" height="5" align="right" valign="top"><img src="imagens/curva-6-baixo-esq.png" alt="canto baixo esquerdo" width="5" height="5" /></td>
		<td height="5" bgcolor="#EBEBEB"></td>
		<td width="5" height="5" align="left" valign="top"><img src="imagens/curva-6-baixo-dir.png" alt="canto baixo direito" width="5" height="5" /></td>
	</tr>
</table>
<?php 
	}
?>
