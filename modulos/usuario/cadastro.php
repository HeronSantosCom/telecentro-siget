<form action="?pm=usuario&amp;ps=cadastro.post" method="post" name="usuario" id="usuario" onSubmit="return validaUsuario();">
	<?php topicodestaque("Controle"); ?>
	<table width="700" border="0" cellspacing="2" cellpadding="0">
		<tr>
			<td width="200" class="linha-fundo">Tipo de Usu&aacute;rio: </td>
			<td><select name="id_tipousuario" id="id_tipousuario" style="width:100%" class="input-destacado" >
					<option value="" selected="selected">Selecione...</option>
					<option value="">-------------------</option>
					<?php 
	// enfilera os tipos de usu�rios
	$sql = mysql_query("select * from tipousuario order by tipousuario asc");
	while ($tipousuario = mysql_fetch_array($sql)) {
		echo "<option value=\"".$tipousuario['id']."\">".$tipousuario['tipousuario']."</option>";
	}
		?>
				</select></td>
		</tr>
	</table>
	<?php topicodestaque("Identifica&ccedil;&atilde;o da pessoa"); ?>
	<table width="700" border="0" cellspacing="2" cellpadding="0">
		<tr>
			<td width="200" class="linha-fundo">Nome:</td>
			<td><input name="nome" type="text" class="input-destacado" id="nome" maxlength="150" style="width:100%" onkeyup="this.value = this.value.toUpperCase();" /></td>
		</tr>
		<tr>
			<td width="200" class="linha-fundo">Data de nascimento: </td>
			<td><table width="30%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><input name="datanascimento" type="text" class="input-destacado" id="datanascimento" maxlength="10" style="width:100%" onkeyup="formataCampo(this, '##/##/####', event);" onkeypress="return bloqueiaAlfa(event);" /></td>
						<td width="2">&nbsp;</td>
						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...')" value="?" /></td>
					</tr>
				</table></td>
		</tr>
		<tr>
			<td width="200" class="linha-fundo">Sexo:</td>
			<td><select name="id_sexo" id="id_sexo" class="input-destacado" style="width:50%" >
					<option value="" selected="selected">Selecione...</option>
					<option value="">-------------------</option>
					<?php 
		// enfilera os sexos
		$sql = mysql_query("select * from sexo order by sexo asc");
		while ($sexo = mysql_fetch_array($sql)) {
			echo "<option value=\"".$sexo['id']."\">".$sexo['sexo']."</option>";
		}
?>
				</select></td>
		</tr>
		<tr>
			<td width="200" class="linha-fundo">Nacionalidade:</td>
			<td><select name="id_nacionalidade" id="id_nacionalidade" class="input-destacado" style="width:50%" >
					<option value="" selected="selected">Selecione...</option>
					<option value="">-------------------</option>
					<?php 
		// enfilera as nacionalidades
		$sql = mysql_query("select * from nacionalidade order by nacionalidade asc");
		while ($nacionalidade = mysql_fetch_array($sql)) {
			echo "<option value=\"".$nacionalidade['id']."\">".$nacionalidade['nacionalidade']."</option>";
		}
?>
				</select></td>
		</tr>
		<tr>
			<td width="200" class="linha-fundo">Pais de origem:</td>
			<td><table width="30%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><input name="paisorigem" type="text" class="input-normal" id="paisorigem" maxlength="15" style="width:100%" onkeyup="this.value = this.value.toUpperCase();" /></td>
						<td width="2">&nbsp;</td>
						<td width="25"><input type="button" class="button-normal" onclick="alert('Somente se for estrangeiro ou naturalizado brasileiro...')" value="?" /></td>
					</tr>
				</table></td>
		</tr>
		<tr>
			<td width="200" class="linha-fundo">Nome completo do pai:</td>
			<td><input name="nomepai" type="text" class="input-normal" id="nomepai" maxlength="150" style="width:100%" onkeyup="this.value = this.value.toUpperCase();"/></td>
		</tr>
		<tr>
			<td width="200" class="linha-fundo">Nome completo da m&atilde;e:</td>
			<td><input name="nomemae" type="text" class="input-normal" id="nomemae" maxlength="150" style="width:100%" onkeyup="this.value = this.value.toUpperCase();"/></td>
		</tr>
		<tr>
			<td width="200" class="linha-fundo">Estado civil:</td>
			<td><select name="id_estadocivil" id="id_estadocivil" class="input-destacado" style="width:50%" >
					<option value="" selected="selected">Selecione...</option>
					<option value="">-------------------</option>
					<?php 
		// enfilera os estados civis
		$sql = mysql_query("select * from estadocivil order by estadocivil asc");
		while ($estadocivil = mysql_fetch_array($sql)) {
			echo "<option value=\"".$estadocivil['id']."\">".$estadocivil['estadocivil']."</option>";
		}
?>
				</select></td>
		</tr>
		<tr>
			<td width="200" class="linha-fundo">Ra&ccedil;a / Cor:</td>
			<td><select name="id_raca" id="id_raca" class="input-destacado" style="width:50%" >
					<option value="" selected="selected">Selecione...</option>
					<option value="">-------------------</option>
					<?php 
		// enfilera as ra&ccedil;as
		$sql = mysql_query("select * from raca order by raca asc");
		while ($raca = mysql_fetch_array($sql)) {
			echo "<option value=\"".$raca['id']."\">".$raca['raca']."</option>";
		}
?>
				</select></td>
		</tr>
		<tr>
			<td width="200" valign="top" class="textomedio-preto">Tipo de deficiencia: </td>
			<td valign="top" class="textomedio-preto"><?php 
		// enfilera os tipos de deficiencias
		$sql = mysql_query("select * from deficiencia");
		while ($deficiencia = mysql_fetch_array($sql)) {
			if ($deficiencia['id'] == "6") {
				echo "<input name=\"usuario_deficiencia_".$deficiencia['id']."\" type=\"checkbox\" id=\"usuario_deficiencia_".$deficiencia['id']."\" value=\"sim\" checked=\"checked\">".$deficiencia['deficiencia']."<br>";
			} else { 
				echo "<input name=\"usuario_deficiencia_".$deficiencia['id']."\" type=\"checkbox\" id=\"usuario_deficiencia_".$deficiencia['id']."\" value=\"sim\">".$deficiencia['deficiencia']."<br>";
			}
		}
?></td>
		</tr>
	</table>
	<?php topicodestaque("Identifica��o do domic�lio"); ?>
	<table width="700" border="0" cellspacing="2" cellpadding="0">
		<tr>
			<td width="200" align="left" class="linha-fundo"> Logradouro:</td>
			<td><input name="enderecologradouro" type="text" class="input-destacado" id="enderecologradouro" maxlength="100" style="width:100%" onkeyup="this.value = this.value.toUpperCase();"/></td>
		</tr>
		<tr>
			<td width="200" align="left" class="linha-fundo">N&uacute;mero:</td>
			<td><table width="30%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><input name="endereconumero" type="text" class="input-destacado" id="endereconumero" style="width:100%"onkeypress="return bloqueiaAlfa(event);" maxlength="7" /></td>
						<td width="2">&nbsp;</td>
						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...')" value="?" /></td>
					</tr>
				</table></td>
		</tr>
		<tr>
			<td width="200" align="left" class="linha-fundo">Complemento:</td>
			<td><input name="enderecocomplemento" type="text" class="input-normal" id="enderecocomplemento" maxlength="50" style="width:100%" onkeyup="this.value = this.value.toUpperCase();"/></td>
		</tr>
		<tr>
			<td width="200" align="left" class="linha-fundo">CEP:</td>
			<td><table width="30%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><input name="enderecocep" type="text" class="input-normal" id="enderecocep" style="width:100%" onkeyup="formataCampo(this, '#####-###', event);" onkeypress="return bloqueiaAlfa(event);" maxlength="9" /></td>
						<td width="2">&nbsp;</td>
						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...')" value="?" /></td>
					</tr>
				</table></td>
		</tr>
		<tr>
			<td width="200" align="left" class="linha-fundo">Bairro:</td>
			<td><select name="endereco_id_bairro" id="endereco_id_bairro" class="input-destacado" style="width:75%" >
					<option value="" selected="selected">Selecione...</option>
					<option value="">-------------------</option>
					<?php 
		// enfilera bairros
		$sql = mysql_query("select * from bairro order by bairro asc");
		while ($bairro = mysql_fetch_array($sql)) {
			echo "<option value=\"".$bairro['id']."\">".$bairro['bairro']."</option>";
		}
?>
				</select></td>
		</tr>
		<tr>
			<td width="200" align="left" class="linha-fundo">Munic&iacute;pio:</td>
			<td><select name="endereco_id_municipio" id="endereco_id_municipio" class="input-destacado" style="width:75%" >
					<option value="" selected="selected">Selecione...</option>
					<option value="">-------------------</option>
					<?php 
		// enfilera municipios
		$sql = mysql_query("select * from municipio order by municipio asc");
		while ($municipio = mysql_fetch_array($sql)) {
			echo "<option value=\"".$municipio['id']."\">".$municipio['municipio']."</option>";
		}
?>
				</select></td>
		</tr>
		<tr>
			<td width="200" align="left" class="linha-fundo">Estado:</td>
			<td><select name="endereco_id_uf" id="endereco_id_uf" class="input-destacado" style="width:50%" >
					<option value="" selected="selected">Selecione...</option>
					<option value="">-------------------</option>
					<?php 
		// enfilera os estados
		$sql = mysql_query("select * from uf order by estado asc");
		while ($uf = mysql_fetch_array($sql)) {
			echo "<option value=\"".$uf['id']."\">".$uf['estado']."</option>";
		}
?>
				</select></td>
		</tr>
		<tr>
			<td width="200" align="left" class="linha-fundo">Tipo de domic&iacute;lio: </td>
			<td><select name="domicilio_id_domiciliotipo" id="domicilio_id_domiciliotipo" class="input-normal" style="width:50%" >
					<option value="" selected="selected">Selecione...</option>
					<option value="">-------------------</option>
					<?php 
		// enfilera tipo domicilio
		$sql = mysql_query("select * from domiciliotipo order by tipocasa asc");
		while ($domiciliotipo = mysql_fetch_array($sql)) {
			echo "<option value=\"".$domiciliotipo['id']."\">".$domiciliotipo['tipocasa']."</option>";
		}
?>
				</select></td>
		</tr>
		<tr>
			<td width="200" align="left" class="linha-fundo">Tipo constru&ccedil;&atilde;o: </td>
			<td><select name="domicilio_id_domicilioconstrucao" id="domicilio_id_domicilioconstrucao" class="input-normal" style="width:50%" >
					<option value="" selected="selected">Selecione...</option>
					<option value="">-------------------</option>
					<?php 
		// enfilera os tipos de construcao
		$sql = mysql_query("select * from domiciliotipoconstrucao order by construcao asc");
		while ($domiciliotipoconstrucao = mysql_fetch_array($sql)) {
			echo "<option value=\"".$domiciliotipoconstrucao['id']."\">".$domiciliotipoconstrucao['construcao']."</option>";
		}
?>
				</select></td>
		</tr>
		<tr>
			<td width="200" align="left" valign="top" class="linha-fundo">Residentes no domic&iacute;lio:</td>
			<td><input name="domicilioresidentes" type="text" class="input-normal" id="domicilioresidentes" style="width:100%"onkeyup="this.value = this.value.toUpperCase();" value="" maxlength="150" /></td>
		</tr>
	</table>
	<?php topicodestaque("Formas de contato"); ?>
	<table width="700" border="0" cellspacing="2" cellpadding="0">
		<tr>
			<td width="200" align="left" class="linha-fundo">Telefone Fixo:</td>
			<td><table width="30%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><input name="telefonefixo" type="text" class="input-destacado" id="telefonefixo" style="width:100%" onkeyup="formataCampo(this, '## ####-####', event);" onkeypress="return bloqueiaAlfa(event);" maxlength="12"/></td>
						<td width="2">&nbsp;</td>
						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...')" value="?" /></td>
					</tr>
				</table></td>
		</tr>
		<tr>
			<td width="200" align="left" class="linha-fundo">Telefone Celular:</td>
			<td><table width="30%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><input name="telefonecelular" type="text" class="input-normal" id="telefonecelular" style="width:100%" onkeyup="formataCampo(this, '## ####-####', event);" onkeypress="return bloqueiaAlfa(event);" maxlength="12"/></td>
						<td width="2">&nbsp;</td>
						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...')" value="?" /></td>
					</tr>
				</table></td>
		</tr>
		<tr>
			<td width="200" align="left" class="linha-fundo">E-mail:</td>
			<td><table width="75%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><input name="email" type="text" class="input-normal" id="email" style="width:100%" onkeyup="this.value = this.value.toLowerCase();" maxlength="100"/></td>
					</tr>
				</table></td>
		</tr>
	</table>
	<?php topicodestaque("Documentos"); ?>
	<table width="700" border="0" cellspacing="2" cellpadding="0">
		<tr>
			<td width="200" align="left" class="linha-fundo">NIS:</td>
			<td><table width="35%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><input name="nis" type="text" class="input-normal" id="nis" style="width:100%" maxlength="11" onkeypress="return bloqueiaAlfa(event);" /></td>
						<td width="2">&nbsp;</td>
						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...')" value="?" /></td>
					</tr>
				</table></td>
		</tr>
		<tr>
			<td width="200" align="left" class="linha-fundo">CPF:</td>
			<td><table width="35%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><input name="cpf" type="text" class="input-destacado" id="cpf" style="width:100%" onkeyup="formataCampo(this, '###.###.###-##', event);" onkeypress="return bloqueiaAlfa(event);" maxlength="14"/></td>
						<td width="2">&nbsp;</td>
						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...\n\nCaso n&atilde;o tenha em m&atilde;os, digite somente *!')" value="?" /></td>
					</tr>
				</table></td>
		</tr>
		<tr>
			<td width="200" class="linha-fundo">Identidade:</td>
			<td><table width="30%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><input name="identidadenumero" type="text" class="input-destacado" id="identidadenumero" style="width:100%" maxlength="11"onkeypress="return bloqueiaAlfa(event);" /></td>
						<td width="2">&nbsp;</td>
						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...\n\nCaso n&atilde;o tenha em m&atilde;os, digite somente *!')" value="?" /></td>
					</tr>
				</table></td>
		</tr>
		<tr>
			<td width="200" class="linha-fundo"><img src="imagens/marcador-normal.png" alt="marcador" width="14" height="14" hspace="2" vspace="0" border="0" align="absmiddle" />Data de emiss&atilde;o:</td>
			<td><table width="30%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><input name="identidadedataemissao" type="text" class="input-normal" id="identidadedataemissao" style="width:100%" onkeyup="formataCampo(this, '##/##/####', event);" onkeypress="return bloqueiaAlfa(event);" maxlength="10"/></td>
						<td width="2">&nbsp;</td>
						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...')" value="?" /></td>
					</tr>
				</table></td>
		</tr>
		<tr>
			<td width="200" class="linha-fundo"><img src="imagens/marcador-normal.png" alt="marcador" width="14" height="14" hspace="2" vspace="0" border="0" align="absmiddle" /><span class="celulaLinha">Org&atilde;o emissor</span>:</td>
			<td><table width="30%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><input name="identidadeorgaoemissor" type="text" class="input-normal" id="identidadeorgaoemissor" style="width:100%" onkeyup="this.value = this.value.toUpperCase();" maxlength="10"/></td>
					</tr>
				</table></td>
		</tr>
		<tr>
			<td width="200" class="linha-fundo">CTPS:</td>
			<td><table width="25%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><input name="ctpsnumero" type="text" class="input-destacado" id="ctpsnumero" style="width:100%"maxlength="7" onkeypress="return bloqueiaAlfa(event);"/></td>
						<td width="2">&nbsp;</td>
						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...\n\nCaso n&atilde;o tenha em m&atilde;os, digite somente *!')" value="?" /></td>
					</tr>
				</table></td>
		</tr>
		<tr>
			<td width="200" class="linha-fundo"><img src="imagens/marcador-normal.png" alt="marcador" width="14" height="14" hspace="2" vspace="0" border="0" align="absmiddle" />S&eacute;rie:</td>
			<td><table width="15%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><input name="ctpsserie" type="text" class="input-destacado" id="ctpsserie" style="width:100%"maxlength="5" onkeypress="return bloqueiaAlfa(event);"/></td>
						<td width="2">&nbsp;</td>
						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...\n\nCaso n&atilde;o tenha em m&atilde;os, digite somente *!')" value="?" /></td>
					</tr>
				</table></td>
		</tr>
		<tr>
			<td width="200" class="linha-fundo"><img src="imagens/marcador-normal.png" alt="marcador" width="14" height="14" hspace="2" vspace="0" border="0" align="absmiddle" />Data de emiss&atilde;o:</td>
			<td><table width="30%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><input name="ctpsdataemissao" type="text" class="input-normal" id="ctpsdataemissao" style="width:100%" onkeyup="formataCampo(this, '##/##/####', event);" onkeypress="return bloqueiaAlfa(event);" maxlength="10"/></td>
						<td width="2">&nbsp;</td>
						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...')" value="?" /></td>
					</tr>
				</table></td>
		</tr>
		<tr>
			<td width="200" class="linha-fundo"><img src="imagens/marcador-normal.png" alt="marcador" width="14" height="14" hspace="2" vspace="0" border="0" align="absmiddle" />Estado:</td>
			<td><select name="ctps_id_uf" id="ctps_id_uf" style="width:50%" class="input-normal" >
					<option value="" selected="selected">Selecione...</option>
					<option value="">-------------------</option>
					<?php 
		// enfilera os uf's da ctps
		$sql = mysql_query("select * from uf order by estado asc");
		while ($uf = mysql_fetch_array($sql)) {
			echo "<option value=\"".$uf['id']."\">".$uf['estado']."</option>";
		}
?>
				</select></td>
		</tr>
		<tr>
			<td width="200" class="linha-fundo">TItulo Eleitor: </td>
			<td><table width="35%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><input name="tituloeleitornumero" type="text" class="input-destacado" id="tituloeleitornumero" style="width:100%"maxlength="13" onkeypress="return bloqueiaAlfa(event);"/></td>
						<td width="2">&nbsp;</td>
						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...\n\nCaso n&atilde;o tenha em m&atilde;os, digite somente *!')" value="?" /></td>
					</tr>
				</table></td>
		</tr>
		<tr>
			<td width="200" class="linha-fundo"><img src="imagens/marcador-normal.png" alt="marcador" width="14" height="14" hspace="2" vspace="0" border="0" align="absmiddle" />Zona:</td>
			<td><table width="15%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><input name="tituloeleitorzona" type="text" class="input-destacado" id="tituloeleitorzona" style="width:100%"maxlength="4" onkeypress="return bloqueiaAlfa(event);"/></td>
						<td width="2">&nbsp;</td>
						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...\n\nCaso n&atilde;o tenha em m&atilde;os, digite somente *!')" value="?" /></td>
					</tr>
				</table></td>
		</tr>
		<tr>
			<td width="200" class="linha-fundo"><img src="imagens/marcador-normal.png" alt="marcador" width="14" height="14" hspace="2" vspace="0" border="0" align="absmiddle" />Se&ccedil;&atilde;o:</td>
			<td><table width="15%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><input name="tituloeleitorsecao" type="text" class="input-destacado" id="tituloeleitorsecao" style="width:100%" maxlength="4" onkeypress="return bloqueiaAlfa(event);"/></td>
						<td width="2">&nbsp;</td>
						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...\n\nCaso n&atilde;o tenha em m&atilde;os, digite somente *!')" value="?" /></td>
					</tr>
				</table></td>
		</tr>
	</table>
	<?php topicodestaque("Qualifica&ccedil;&atilde;o escolar"); ?>
	<table width="700" border="0" cellspacing="2" cellpadding="0">
		<tr>
			<td width="200" class="linha-fundo">Frequenta Escola:</td>
			<td><select name="escola_id_escolatipo" id="escola_id_escolatipo" class="input-destacado" style="width:50%" >
					<option value="" selected="selected">Selecione...</option>
					<option value="">-------------------</option>
					<?php 
		// enfilera tipo de escola
		$sql = mysql_query("select * from escolatipo order by tipo asc");
		while ($escolatipo = mysql_fetch_array($sql)) {
			echo "<option value=\"".$escolatipo['id']."\">".$escolatipo['tipo']."</option>";
		}
?>
				</select></td>
		</tr>
		<tr>
			<td width="200" class="linha-fundo">Grau de instru&ccedil;&atilde;o:</td>
			<td><select name="escola_id_escolagrau" id="escola_id_escolagrau" class="input-destacado" style="width:100%" >
					<option value="" selected="selected">Selecione...</option>
					<option value="">-------------------</option>
					<?php 
		// enfilera os graus escolares
		$sql = mysql_query("select * from escolagrau order by grau asc");
		while ($escolagrau = mysql_fetch_array($sql)) {
			echo "<option value=\"".$escolagrau['id']."\">".$escolagrau['grau']."</option>";
		}
?>
				</select></td>
		</tr>
		<tr>
			<td width="200" class="linha-fundo">S&eacute;rie escolar:</td>
			<td><select name="escola_id_escolaserie" id="escola_id_escolaserie" class="input-destacado" style="width:100%" >
					<option value="" selected="selected">Selecione...</option>
					<option value="">-------------------</option>
					<?php 
		// enfilera as s&eacute;ries escolares
		$sql = mysql_query("select * from escolaserie");
		while ($escolaserie = mysql_fetch_array($sql)) {
			echo "<option value=\"".$escolaserie['id']."\">".$escolaserie['serie']."</option>";
		}
?>
				</select></td>
		</tr>
		<tr>
			<td width="200" class="linha-fundo">Turno:</td>
			<td><select name="escola_id_escolaturno" id="escola_id_escolaturno" class="input-destacado" style="width:50%" >
					<option value="" selected="selected">Selecione...</option>
					<option value="">-------------------</option>
					<?php 
		// enfilera turnos
		$sql = mysql_query("select * from escolaturno");
		while ($escolaturno = mysql_fetch_array($sql)) {
			echo "<option value=\"".$escolaturno['id']."\">".$escolaturno['turno']."</option>";
		}
?>
				</select></td>
		</tr>
		<tr>
			<td width="200" class="linha-fundo">Nome da escola:</td>
			<td><select name="escola_id_escolanome" id="escola_id_escolanome" class="input-destacado"style="width:100%" >
					<option value="" selected="selected">Selecione...</option>
					<option value="">-------------------</option>
					<?php 
		// enfilera nomes das escolas
		$sql = mysql_query("select * from escolanome order by nome asc");
		while ($escolanome = mysql_fetch_array($sql)) {
			echo "<option value=\"".$escolanome['Id']."\">".$escolanome['nome']."</option>";
		}
?>
				</select></td>
		</tr>
	</table>
	<?php topicodestaque("Qualifica&ccedil;&atilde;o profissional"); ?>
	<table width="700" border="0" cellspacing="2" cellpadding="0">
		<tr>
			<td align="left" class="linha-fundo">Situa&ccedil;&atilde;o no trabalho:</td>
			<td><select name="emprego_id_empregosituacao" id="emprego_id_empregosituacao" class="input-destacado" style="width:100%" >
					<option value="" selected="selected">Selecione...</option>
					<option value="">-------------------</option>
					<?php 
		// enfilera situacao do emprego
		$sql = mysql_query("select * from empregosituacao order by situacao asc");
		while ($empregosituacao = mysql_fetch_array($sql)) {
			echo "<option value=\"".$empregosituacao['id']."\">".$empregosituacao['situacao']."</option>";
		}
?>
				</select></td>
		</tr>
		<tr>
			<td align="left" class="linha-fundo"><img src="imagens/marcador-normal.png" alt="marcador" width="14" height="14" hspace="2" vspace="0" border="0" align="absmiddle" />Nome da empresa:</td>
			<td><table width="75%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><input name="empregoempresa" type="text" class="input-destacado" id="empregoempresa" style="width:100%" onkeyup="this.value = this.value.toUpperCase();" maxlength="150"/></td>
						<td width="2">&nbsp;</td>
						<td width="25"><input type="button" class="button-normal" onclick="alert('Se desempregao, especificar o �ltimo emprego...')" value="?" /></td>
					</tr>
				</table></td>
		</tr>
		<tr>
			<td align="left" class="linha-fundo"><img src="imagens/marcador-normal.png" alt="marcador" width="14" height="14" hspace="2" vspace="0" border="0" align="absmiddle" />Data de admiss&atilde;o:</td>
			<td><table width="30%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><input name="empregodataadmissao" type="text" class="input-normal" id="empregodataadmissao" style="width:100%" onkeypress="formataCampo(this, '##/##/####', event);return bloqueiaAlfa(event);" maxlength="10"/></td>
						<td width="2">&nbsp;</td>
						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...')" value="?" /></td>
					</tr>
				</table></td>
		</tr>
		<tr>
			<td align="left" class="linha-fundo"><img src="imagens/marcador-normal.png" alt="marcador" width="14" height="14" hspace="2" vspace="0" border="0" align="absmiddle" />Ocupa&ccedil;&atilde;o:</td>
			<td><input name="empregoocupacao" type="text" class="input-normal" id="empregoocupacao" style="width:50%"onkeyup="this.value = this.value.toUpperCase();" maxlength="50"/></td>
		</tr>
		<tr>
			<td align="left" class="linha-fundo"><img src="imagens/marcador-normal.png" alt="marcador" width="14" height="14" hspace="2" vspace="0" border="0" align="absmiddle" />Remunera&ccedil;&atilde;o:</td>
			<td><table width="30%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="20" class="textomedio-preto">R$</td>
						<td><input name="empregoremuneracao" type="text" class="input-normal" id="empregoremuneracao" style="width:100%; text-align:right" onkeydown="formataCurrency(this,10,event,2)" onkeypress="return bloqueiaAlfa(event);" maxlength="10"/></td>
						<td width="2">&nbsp;</td>
						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...')" value="?" /></td>
					</tr>
				</table></td>
		</tr>
		<tr>
			<td align="left" class="linha-fundo">Renda de aposen./pens&atilde;o:</td>
			<td><table width="30%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="20" class="textomedio-preto">R$</td>
						<td><input name="rendaaposentadoria" type="text" class="input-normal" id="rendaaposentadoria" style="width:100%; text-align:right" onkeydown="formataCurrency(this,10,event,2)" onkeypress="return bloqueiaAlfa(event);" maxlength="13"/></td>
						<td width="2">&nbsp;</td>
						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...')" value="?" /></td>
					</tr>
				</table></td>
		</tr>
		<tr>
			<td align="left" class="linha-fundo">Renda de seguro-desemprego:</td>
			<td><table width="30%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="20" class="textomedio-preto">R$</td>
						<td><input name="rendaseguro" type="text" class="input-normal" id="rendaseguro" style="width:100%; text-align:right" onkeydown="formataCurrency(this,10,event,2)" onkeypress="return bloqueiaAlfa(event);" maxlength="13"/></td>
						<td width="2">&nbsp;</td>
						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...')" value="?" /></td>
					</tr>
				</table></td>
		</tr>
		<tr>
			<td align="left" class="linha-fundo">Renda de pens&atilde;o aliment&iacute;cia:</td>
			<td><table width="30%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="20" class="textomedio-preto">R$</td>
						<td><input name="rendapensao" type="text" class="input-normal" id="rendapensao" style="width:100%; text-align:right" onkeydown="formataCurrency(this,10,event,2)" onkeypress="return bloqueiaAlfa(event);" maxlength="13"/></td>
						<td width="2">&nbsp;</td>
						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...')" value="?" /></td>
					</tr>
				</table></td>
		</tr>
		<tr>
			<td align="left" class="linha-fundo">Outras rendas:</td>
			<td><table width="30%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="20" class="textomedio-preto">R$</td>
						<td><input name="rendaoutras" type="text" class="input-normal" id="rendaoutras" style="width:100%; text-align:right" onkeydown="formataCurrency(this,10,event,2)" onkeypress="return bloqueiaAlfa(event);" maxlength="13"/></td>
						<td width="2">&nbsp;</td>
						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...')" value="?" /></td>
					</tr>
				</table></td>
		</tr>
		<tr>
			<td width="200" valign="top" class="textomedio-preto">Participa de algum programa do Governo Federal ou  recebe algum benef&iacute;cio social?</td>
			<td valign="top"><?php 
		// enfilera os programas de governo
		$sql = mysql_query("select * from programagoverno");
		while ($programagoverno = mysql_fetch_array($sql)) {
			if ($programagoverno['id'] == "10") {
				echo "<input name=\"usuario_programagoverno_".$programagoverno['id']."\" type=\"checkbox\" id=\"usuario_programagoverno_".$programagoverno['id']."\" value=\"sim\" checked=\"checked\">".$programagoverno['programa']."<br>";
			} else { 
				echo "<input name=\"usuario_programagoverno_".$programagoverno['id']."\" type=\"checkbox\" id=\"usuario_programagoverno_".$programagoverno['id']."\" value=\"sim\">".$programagoverno['programa']."<br>";
			}
		}
?></td>
		</tr>
	</table>
	<br />
	<table width="450" height="25" border="0" cellspacing="2" cellpadding="0">
		<tr>
			<td align="center" bgcolor="#FFCCCC" class="linha-fundodireito">Todos os campos em <strong>rosa beb&ecirc;</strong>  s&atilde;o obrigat&oacute;rios!</td>
		</tr>
	</table>
	<br />
	<table width="700" border="0" cellspacing="3" cellpadding="0">
		<tr>
			<td width="35%"><input type="button" class="button-normal" value="Cancelar" onClick="javascript:carregapagina('?pm=usuario','_self')" /></td>
			<td width="65%"><input name="post" type="submit" class="button-destacado" id="post" value="Cadastrar" /></td>
		</tr>
	</table>
	<input name="codigo_telecentro" type="hidden" id="codigo_telecentro" value="<?php echo $codigotelecentro; ?>">
</form>
