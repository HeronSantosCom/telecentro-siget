<?php topicodestaque("Consulta de usu&aacute;rio"); ?>
<form id="consulta" name="consulta" method="post" action="?pm=usuario&amp;ps=consulta.result" onSubmit="return validaConsultaUsuario();">
	<table border="0" cellpadding="0" cellspacing="2">
		<tr>
			<td class="linha-fundo">Nome ou matr&iacute;cula:</td>
			<td width="400"><input name="busca" type="text" class="input-destacado" id="busca" style="width:100%" onkeyup="this.value = this.value.toUpperCase();" /></td>
			<td width="25"><input type="button" class="button-normal" onclick="alert('Pesquisar por matrícula ou por nome...')" value="?" /></td>
		</tr>
		<tr>
			<td colspan="3" align="center" class="textopequeno-preto">Digite a matr&iacute;cula ou nome a ser consultado.<br />
				Exemplo: JOAO ou 00100001-1</td>
		</tr>
	</table>
	<br />
	<table width="450" border="0" cellspacing="3" cellpadding="0">
		<tr>
			<td width="35%"><input type="button" class="button-normal" value="Cancelar" onClick="javascript:carregapagina('?pm=usuario','_self')" /></td>
			<td width="65%"><input name="post" type="submit" class="button-destacado" id="post" value="Consultar"/></td>
		</tr>
	</table>
</form>
