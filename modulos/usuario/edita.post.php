<br />
<?php 
	// resgata os campos no formulário
	$id_usuario 								= $_POST["id_usuario"];
	$codigo_telecentro						= $_POST["codigo_telecentro"];
	$id_tipousuario							= $_POST["id_tipousuario"];
	$matricula									= $_POST["matricula"];
	$datainscricao								= $_POST["datainscricao"];
	$nome											= $_POST["nome"];
	$datanascimento							= conversordata($_POST["datanascimento"], "-", "normal.mysql");
	$id_sexo										= $_POST["id_sexo"];
	$id_nacionalidade							= $_POST["id_nacionalidade"];
	$paisorigem									= $_POST["paisorigem"];
	$nomepai										= $_POST["nomepai"];
	$nomemae										= $_POST["nomemae"];
	$id_estadocivil							= $_POST["id_estadocivil"];
	$id_raca										= $_POST["id_raca"];
	$nis											= $_POST["nis"];
	if ($_POST["identidadenumero"] == "*") { $identidadenumero = "0"; } else { $identidadenumero = $_POST["identidadenumero"]; }
	$identidadeorgaoemissor					= $_POST["identidadeorgaoemissor"]; 
	$identidadedataemissao					= conversordata($_POST["identidadedataemissao"], "-", "normal.mysql");
	if ($_POST["ctpsnumero"] == "*") { $ctpsnumero = "0"; } else { $ctpsnumero = $_POST["ctpsnumero"]; }
	if ($_POST["ctpsserie"] == "*") { $ctpsserie = "0"; } else { $ctpsserie = $_POST["ctpsserie"]; }
	$ctpsdataemissao							= conversordata($_POST["ctpsdataemissao"], "-", "normal.mysql");
	$ctps_id_uf									= $_POST["ctps_id_uf"];
	if ($_POST["cpf"] == "*") { $cpf = "0"; } else { $cpf = $_POST["cpf"]; }
	if ($_POST["tituloeleitornumero"] == "*") { $tituloeleitornumero = "0"; } else { $tituloeleitornumero = $_POST["tituloeleitornumero"]; }
	if ($_POST["tituloeleitorzona"] == "*") { $tituloeleitorzona = "0"; } else { $tituloeleitorzona = $_POST["tituloeleitorzona"]; }
	if ($_POST["tituloeleitorsecao"] == "*") { $tituloeleitorsecao = "0"; } else { $tituloeleitorsecao = $_POST["tituloeleitorsecao"]; }
	$escola_id_escolatipo					= $_POST["escola_id_escolatipo"];
	$escola_id_escolagrau					= $_POST["escola_id_escolagrau"];
	$escola_id_escolaserie					= $_POST["escola_id_escolaserie"];
	$escola_id_escolaturno					= $_POST["escola_id_escolaturno"];
	$escola_id_escolanome					= $_POST["escola_id_escolanome"];
	$emprego_id_empregosituacao			= $_POST["emprego_id_empregosituacao"];
	$empregoempresa							= $_POST["empregoempresa"];
	$empregodataadmissao						= conversordata($_POST["empregodataadmissao"], "-", "normal.mysql");
	$empregoocupacao							= $_POST["empregoocupacao"];
	$empregoremuneracao						= $_POST["empregoremuneracao"];
	$rendaaposentadoria						= $_POST["rendaaposentadoria"];
	$rendaseguro								= $_POST["rendaseguro"];
	$rendapensao								= $_POST["rendapensao"];
	$rendaoutras								= $_POST["rendaoutras"];
	// soma todas as rendas
	$rendatotal_temporario					= $empregoremuneracao + $rendaaposentadoria +  $rendaseguro + $rendapensao + $rendaoutras;
	$rendatotal									= number_format($rendatotal_temporario, 2,'.','');
	$enderecologradouro						= $_POST["enderecologradouro"];
	$endereconumero							= $_POST["endereconumero"];
	$enderecocomplemento						= $_POST["enderecocomplemento"];
	$enderecocep								= $_POST["enderecocep"];
	$endereco_id_bairro						= $_POST["endereco_id_bairro"];
	$endereco_id_municipio					= $_POST["endereco_id_municipio"];
	$endereco_id_uf							= $_POST["endereco_id_uf"];
	$telefonefixo								= $_POST["telefonefixo"];
	$telefonecelular								= $_POST["telefonecelular"];
	$email										= $_POST["email"];
	$domicilio_id_domiciliotipo			= $_POST["domicilio_id_domiciliotipo"];
	$domicilio_id_domicilioconstrucao	= $_POST["domicilio_id_domicilioconstrucao"];
	$domicilioresidentes						= $_POST["domicilioresidentes"];
	if (empty($_POST["usuario_deficiencia_1"])) { $usuario_deficiencia_1 = "nao"; } else { $usuario_deficiencia_1 = "sim"; }
	if (empty($_POST["usuario_deficiencia_2"])) { $usuario_deficiencia_2 = "nao"; } else { $usuario_deficiencia_2 = "sim"; }
	if (empty($_POST["usuario_deficiencia_3"])) { $usuario_deficiencia_3 = "nao"; } else { $usuario_deficiencia_3 = "sim"; }
	if (empty($_POST["usuario_deficiencia_4"])) { $usuario_deficiencia_4 = "nao"; } else { $usuario_deficiencia_4 = "sim"; }
	if (empty($_POST["usuario_deficiencia_5"])) { $usuario_deficiencia_5 = "nao"; } else { $usuario_deficiencia_5 = "sim"; }
	if (empty($_POST["usuario_deficiencia_6"])) { $usuario_deficiencia_6 = "nao"; } else { $usuario_deficiencia_6 = "sim"; }
	if (empty($_POST["usuario_deficiencia_7"])) { $usuario_deficiencia_7 = "nao"; } else { $usuario_deficiencia_7 = "sim"; }
	if (empty($_POST["usuario_programagoverno_1"])) { $usuario_programagoverno_1 = "nao"; } else { $usuario_programagoverno_1 = "sim"; }
	if (empty($_POST["usuario_programagoverno_2"])) { $usuario_programagoverno_2 = "nao"; } else { $usuario_programagoverno_2 = "sim"; }
	if (empty($_POST["usuario_programagoverno_3"])) { $usuario_programagoverno_3 = "nao"; } else { $usuario_programagoverno_3 = "sim"; }
	if (empty($_POST["usuario_programagoverno_4"])) { $usuario_programagoverno_4 = "nao"; } else { $usuario_programagoverno_4 = "sim"; }
	if (empty($_POST["usuario_programagoverno_5"])) { $usuario_programagoverno_5 = "nao"; } else { $usuario_programagoverno_5 = "sim"; }
	if (empty($_POST["usuario_programagoverno_6"])) { $usuario_programagoverno_6 = "nao"; } else { $usuario_programagoverno_6 = "sim"; }
	if (empty($_POST["usuario_programagoverno_7"])) { $usuario_programagoverno_7 = "nao"; } else { $usuario_programagoverno_7 = "sim"; }
	if (empty($_POST["usuario_programagoverno_8"])) { $usuario_programagoverno_8 = "nao"; } else { $usuario_programagoverno_8 = "sim"; }
	if (empty($_POST["usuario_programagoverno_9"])) { $usuario_programagoverno_9 = "nao"; } else { $usuario_programagoverno_9 = "sim"; }
	if (empty($_POST["usuario_programagoverno_10"])) { $usuario_programagoverno_10 = "nao"; } else { $usuario_programagoverno_10 = "sim"; }
	$sql = "UPDATE
					usuario
				SET
					id											= '$id_usuario',
					codigo_telecentro						= '$codigo_telecentro',
					id_tipousuario							= '$id_tipousuario',
					matricula								= '$matricula',
					datainscricao							= '$datainscricao',
					nome										= '$nome',
					datanascimento							= '$datanascimento',
					id_sexo									= '$id_sexo',
					id_nacionalidade						= '$id_nacionalidade',
					paisorigem								= '$paisorigem',
					nomepai									= '$nomepai',
					nomemae									= '$nomemae',
					id_estadocivil							= '$id_estadocivil',
					id_raca									= '$id_raca',
					nis										= '$nis',
					identidadenumero						= '$identidadenumero',
					identidadeorgaoemissor				= '$identidadeorgaoemissor',
					identidadedataemissao				= '$identidadedataemissao',
					ctpsnumero								= '$ctpsnumero',
					ctpsserie								= '$ctpsserie',
					ctpsdataemissao						= '$ctpsdataemissao',
					ctps_id_uf								= '$ctps_id_uf',
					cpf										= '$cpf',
					tituloeleitornumero					= '$tituloeleitornumero',
					tituloeleitorzona						= '$tituloeleitorzona',
					tituloeleitorsecao					= '$tituloeleitorsecao',
					escola_id_escolatipo					= '$escola_id_escolatipo',
					escola_id_escolagrau					= '$escola_id_escolagrau',
					escola_id_escolaserie				= '$escola_id_escolaserie',
					escola_id_escolaturno				= '$escola_id_escolaturno',
					escola_id_escolanome					= '$escola_id_escolanome',
					emprego_id_empregosituacao			= '$emprego_id_empregosituacao',
					empregoempresa							= '$empregoempresa',
					empregodataadmissao					= '$empregodataadmissao',
					empregoocupacao						= '$empregoocupacao',
					empregoremuneracao					= '$empregoremuneracao',
					rendaaposentadoria					= '$rendaaposentadoria',
					rendaseguro								= '$rendaseguro',
					rendapensao								= '$rendapensao',
					rendaoutras								= '$rendaoutras',
					rendatotal								= '$rendatotal',
					enderecologradouro					= '$enderecologradouro',
					endereconumero							= '$endereconumero',
					enderecocomplemento					= '$enderecocomplemento',
					enderecocep								= '$enderecocep',
					endereco_id_bairro					= '$endereco_id_bairro',
					endereco_id_municipio				= '$endereco_id_municipio',
					endereco_id_uf							= '$endereco_id_uf',
					telefonefixo							= '$telefonefixo',
					telefonecelular						= '$telefonecelular',
					email										= '$email',
					domicilio_id_domiciliotipo			= '$domicilio_id_domiciliotipo',
					domicilio_id_domicilioconstrucao	= '$domicilio_id_domicilioconstrucao',
					domicilioresidentes					= '$domicilioresidentes'
				WHERE id='$id_usuario'";

	// se ocorrer tudo certo
	if ($updateUsuario = mysql_query($sql)) {
		$updateDeficiencia = mysql_query("UPDATE usuario_deficiencia SET assinalado='$usuario_deficiencia_1' WHERE id_deficiencia='1' AND matricula_usuario='$matricula'");
		$updateDeficiencia = mysql_query("UPDATE usuario_deficiencia SET assinalado='$usuario_deficiencia_2' WHERE id_deficiencia='2' AND matricula_usuario='$matricula'");
		$updateDeficiencia = mysql_query("UPDATE usuario_deficiencia SET assinalado='$usuario_deficiencia_3' WHERE id_deficiencia='3' AND matricula_usuario='$matricula'");
		$updateDeficiencia = mysql_query("UPDATE usuario_deficiencia SET assinalado='$usuario_deficiencia_4' WHERE id_deficiencia='4' AND matricula_usuario='$matricula'");
		$updateDeficiencia = mysql_query("UPDATE usuario_deficiencia SET assinalado='$usuario_deficiencia_5' WHERE id_deficiencia='5' AND matricula_usuario='$matricula'");
		$updateDeficiencia = mysql_query("UPDATE usuario_deficiencia SET assinalado='$usuario_deficiencia_6' WHERE id_deficiencia='6' AND matricula_usuario='$matricula'");
		$updateDeficiencia = mysql_query("UPDATE usuario_deficiencia SET assinalado='$usuario_deficiencia_7' WHERE id_deficiencia='7' AND matricula_usuario='$matricula'");
		$updateProgramagoverno = mysql_query("UPDATE usuario_programagoverno SET assinalado='$usuario_programagoverno_1' WHERE id_programagoverno='1' AND matricula_usuario='$matricula'");
		$updateProgramagoverno = mysql_query("UPDATE usuario_programagoverno SET assinalado='$usuario_programagoverno_2' WHERE id_programagoverno='2' AND matricula_usuario='$matricula'");
		$updateProgramagoverno = mysql_query("UPDATE usuario_programagoverno SET assinalado='$usuario_programagoverno_3' WHERE id_programagoverno='3' AND matricula_usuario='$matricula'");
		$updateProgramagoverno = mysql_query("UPDATE usuario_programagoverno SET assinalado='$usuario_programagoverno_4' WHERE id_programagoverno='4' AND matricula_usuario='$matricula'");
		$updateProgramagoverno = mysql_query("UPDATE usuario_programagoverno SET assinalado='$usuario_programagoverno_5' WHERE id_programagoverno='5' AND matricula_usuario='$matricula'");
		$updateProgramagoverno = mysql_query("UPDATE usuario_programagoverno SET assinalado='$usuario_programagoverno_6' WHERE id_programagoverno='6' AND matricula_usuario='$matricula'");
		$updateProgramagoverno = mysql_query("UPDATE usuario_programagoverno SET assinalado='$usuario_programagoverno_7' WHERE id_programagoverno='7' AND matricula_usuario='$matricula'");
		$updateProgramagoverno = mysql_query("UPDATE usuario_programagoverno SET assinalado='$usuario_programagoverno_8' WHERE id_programagoverno='8' AND matricula_usuario='$matricula'");
		$updateProgramagoverno = mysql_query("UPDATE usuario_programagoverno SET assinalado='$usuario_programagoverno_9' WHERE id_programagoverno='9' AND matricula_usuario='$matricula'");
		$updateProgramagoverno = mysql_query("UPDATE usuario_programagoverno SET assinalado='$usuario_programagoverno_10' WHERE id_programagoverno='10' AND matricula_usuario='$matricula'");
?>
<table width="350" border="0" cellpadding="0" cellspacing="0" id="status">
	<tr>
		<td width="5" height="5" align="right" valign="bottom"><img src="imagens/curva-6-cima-esq.png" alt="canto cima esquerdo" width="5" height="5" /></td>
		<td height="5" bgcolor="#EBEBEB"></td>
		<td width="5" height="5" align="left" valign="bottom"><img src="imagens/curva-6-cima-dir.png" alt="canto cima direito" width="5" height="5" /></td>
	</tr>
	<tr>
		<td width="5" bgcolor="#EBEBEB"></td>
		<td align="center" bgcolor="#EBEBEB"><table width="100%" border="0" cellspacing="10" cellpadding="0">
				<tr>
					<td width="48" height="48"><img src="imagens/concluido.png" width="48" height="48" /></td>
					<td align="center" class="textogrande-preto"><strong class="textoextragrande-preto">Edi&ccedil;&atilde;o de  Usu&aacute;rio</strong></td>
				</tr>
				<tr>
					<td valign="top">&nbsp;</td>
					<td align="center" valign="top" class="textomedio-preto">Usu&aacute;rio alterado com sucesso!<br />
						<br />
						<span class="padraoMedio"><?php echo $nome ?><br />
						Matr&iacute;cula: <font color="#FF0000"><?php echo $matricula ?></font></span><br />
						<br />
						<table width="100%" border="0" cellspacing="3" cellpadding="0">
							<tr>
								<td width="65%"><input type="button" class="button-destacado" value="Voltar" onClick="javascript:carregapagina('?pm=usuario','_self')" /></td>
							</tr>
						</table></td>
				</tr>
			</table></td>
		<td width="5" bgcolor="#EBEBEB"></td>
	</tr>
	<tr>
		<td width="5" height="5" align="right" valign="top"><img src="imagens/curva-6-baixo-esq.png" alt="canto baixo esquerdo" width="5" height="5" /></td>
		<td height="5" bgcolor="#EBEBEB"></td>
		<td width="5" height="5" align="left" valign="top"><img src="imagens/curva-6-baixo-dir.png" alt="canto baixo direito" width="5" height="5" /></td>
	</tr>
</table>
<?php
	// caso de erro
	} else {
?>
<table width="350" border="0" cellpadding="0" cellspacing="0" id="status">
	<tr>
		<td width="5" height="5" align="right" valign="bottom"><img src="imagens/curva-6-cima-esq.png" alt="canto cima esquerdo" width="5" height="5" /></td>
		<td height="5" bgcolor="#EBEBEB"></td>
		<td width="5" height="5" align="left" valign="bottom"><img src="imagens/curva-6-cima-dir.png" alt="canto cima direito" width="5" height="5" /></td>
	</tr>
	<tr>
		<td width="5" bgcolor="#EBEBEB"></td>
		<td align="center" bgcolor="#EBEBEB"><table width="100%" border="0" cellspacing="10" cellpadding="0">
				<tr>
					<td width="48" height="48"><img src="imagens/exclamacao.png" alt="exclamacao" width="48" height="48" /></td>
					<td align="center" class="textogrande-preto"><strong class="textoextragrande-preto">Edi&ccedil;&atilde;o de Usu&aacute;rio</strong></td>
				</tr>
				<tr>
					<td valign="top">&nbsp;</td>
					<td align="center" valign="top" class="textomedio-preto">Ocorreu um erro ao editar!<br />
						<br />
						Consulte	o	administrador	do	sistema para poder solucionar. <br />
						<br />
						<table width="100%" border="0" cellspacing="3" cellpadding="0">
							<tr>
								<td width="65%"><input type="button" class="button-destacado" value="Voltar" onClick="javascript:carregapagina('javascript:history.go(-1)','_self')" /></td>
							</tr>
						</table></td>
				</tr>
			</table></td>
		<td width="5" bgcolor="#EBEBEB"></td>
	</tr>
	<tr>
		<td width="5" height="5" align="right" valign="top"><img src="imagens/curva-6-baixo-esq.png" alt="canto baixo esquerdo" width="5" height="5" /></td>
		<td height="5" bgcolor="#EBEBEB"></td>
		<td width="5" height="5" align="left" valign="top"><img src="imagens/curva-6-baixo-dir.png" alt="canto baixo direito" width="5" height="5" /></td>
	</tr>
</table>
<?php
	}
?>
