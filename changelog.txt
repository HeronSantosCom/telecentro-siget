
	SiGeT
		Sistema de Gerenciamento dos Telecentros de Nova Igua�u

		Desenvolvimento:
			- Heron Reis dos Santos

		Atualiza��es:
			1.8a - Criado o di�rio de classe;
			1.7a - Modificado alguns dados do banco de dados;
			1.6a - Criado o m�dulo de controle de turmas;
			1.5a - Criado o m�dulo de controle dos estagi�rios;
			1.4a - Aprimorado o m�dulo controlador de Perfis de Login;
			1.3a - Aprimorado o sistema de autentica��o com defini��o de n�vel;
			1.2a - Criado o m�dulo de controle dos usu�rios;
			1.1a - Criado o m�dulo de autentica��o e perfil de login;
			1.0a - Criado a estrutura base;
			
	Prefeitura da Cidade de Nova Igua�u
	CIC - Centro para Inova��o e Competitividade