<?php 
	// index.php
	
	// carrega configura��o
	require("include.configuracao.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?php echo $sistemanome ?></title>
<link href="include.estilo.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" type="text/javascript" src="include.javascript.js"></script>
</head>
<body>
<script language="JavaScript" type="text/javascript">
	if(bw.bw) document.write('<div id="carregapagina"><table width="100%" height="100%" align="center" valign="middle"><tr><td align="center" valign="middle"><p><strong><img src="imagens/carregando.png" alt="carregando" width="48" height="48" /></strong></p><p class="textoextragrande-preto"><strong>Carregando...</strong></p></td></tr></table></div>')
</script>
<table width="99%" border="0" align="center" cellpadding="0" cellspacing="0">
<?php 
	if (empty($statuslogin)) {
?>
	<tr>
		<td align="center"><table border="0" cellpadding="0" cellspacing="0" id="menu">
				<tr>
					<td width="5"></td>
					<?php 
		// cria o menu com os itens definidos no include.configuracao.php
		for($xmenu=0;$xmenu<count($menutitulo);$xmenu++) {
			if ($_SESSION["id_acessonivel"] <= $menunivel[$xmenu]) {
				if ($paginamatriz == $menulink[$xmenu]) { // se for a p�gina atual ou selecionada, colore a aba
?>
					<td valign="bottom"><table border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td align="right" valign="bottom"><img src="imagens/curva-5-cima-esq.png" alt="curva" /></td>
								<td bgcolor="#FF9900"></td>
								<td align="left" valign="bottom"><img src="imagens/curva-5-cima-dir.png" alt="curva" /></td>
							</tr>
							<tr bgcolor="#FF9900">
								<td bgcolor="#FF9900"></td>
								<td align="center" valign="middle" bgcolor="#FF9900" class="textomedio-branco"><a href="?pm=<?php echo $menulink[$xmenu]; ?>"><font color="#000000"><?php echo $menutitulo[$xmenu]; ?></font></a></td>
								<td bgcolor="#FF9900"></td>
							</tr>
						</table></td>
					<td width="5"></td>
					<?php
				} else { // sen�o deixa normal
?>
					<td valign="bottom"><table border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td align="right" valign="bottom"><img src="imagens/curva-4-cima-esq.png" alt="curva" /></td>
								<td bgcolor="#FFCC33"></td>
								<td align="left" valign="bottom"><img src="imagens/curva-4-cima-dir.png" alt="curva" /></td>
							</tr>
							<tr bgcolor="#FFCC33">
								<td bgcolor="#FFCC33"></td>
								<td align="center" valign="middle" bgcolor="#FFCC33" class="textomedio-preto"><a href="?pm=<?php echo $menulink[$xmenu]; ?>"><font color="#000000"><?php echo $menutitulo[$xmenu]; ?></font></a></td>
								<td bgcolor="#FFCC33"></td>
							</tr>
						</table></td>
					<td width="5"></td>
					<?php
				}
			}
		}
?>
				</tr>
			</table></td>
	</tr>
<?php 
	}
?>
	<tr>
		<td bgcolor="#000000"><table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td align="right" valign="bottom"><img src="imagens/curva-5-cima-esq.png" alt="curva" /></td>
					<td bgcolor="#FF9900"></td>
					<td align="left" valign="bottom"><img src="imagens/curva-5-cima-dir.png" alt="curva" /></td>
				</tr>
				<tr bgcolor="#FF9900">
					<td bgcolor="#FF9900"></td>
					<td width="100%" bgcolor="#FF9900"><table height="24" border="0" cellpadding="0" cellspacing="0" id="pagina">
							<tr>
								<td width="24" height="24" align="center" valign="middle"><img src="imagens/marcador-normal.png" width="14" height="14" /></td>
								<td class="textoextragrande-branco"><strong><?php echo $titulopagina; ?></strong></td>
							</tr>
						</table>
						<?php
	// se existir sub p�ginas
	if (array_key_exists("ps",$_GET)) {
?>
						<table height="24" border="0" cellpadding="0" cellspacing="0" id="pagina">
							<tr>
								<td width="25"></td>
								<td width="24" height="24" align="center" valign="middle"><img src="imagens/marcador-destacado.png" width="14" height="14" /></td>
								<td class="textogrande-preto"><strong><?php echo $titulosubpagina; ?></strong></td>
							</tr>
						</table>
						<?php
	}
?></td>
					<td bgcolor="#FF9900"></td>
				</tr>
				<tr>
					<td align="right" valign="bottom"><img src="imagens/curva-5-baixo-esq.png" alt="curva" /></td>
					<td bgcolor="#FF9900"></td>
					<td align="left" valign="bottom"><img src="imagens/curva-5-baixo-dir.png" alt="curva" /></td>
				</tr>
			</table></td>
	</tr>
	<tr>
		<td height="5"></td>
	</tr>
	<tr>
		<td height="200" align="center" valign="top" class="textomedio-preto"><?php
	// carrega a p�gina
	include($carregapagina);
?></td>
	</tr>
</table>
</body>
</html>
