// JavaScript Document

	// include.javascript.js
	
	// abre console
	function carregaconsole() {
		var largura = screen.width;
		var altura = screen.height;
		var esquerda = ((screen.width - largura) / 2);
		var topo = ((screen.height - altura) / 2) - 50;
		window.open('index.php?carrega=console', 'console', 'width='+largura+', height='+altura+', top='+topo+', left='+esquerda+', fullscreen=1, scrollbars=no, resizable=no');
		window.close();
	}

	// carregando, by Thomas Brattli de www.bratta.com, 1999
	function checkBrowser(){
		this.ver=navigator.appVersion
		this.dom=document.getElementById?1:0
		this.ie5=(this.ver.indexOf("MSIE 5")>-1 && this.dom)?1:0;
		this.ie4=(document.all && !this.dom)?1:0;
		this.ns5=(this.dom && parseInt(this.ver) >= 5) ?1:0;
		this.ns4=(document.layers && !this.dom)?1:0;
		this.bw=(this.ie5 || this.ie4 || this.ns4 || this.ns5)
		return this
	}
	bw=new checkBrowser()
	function hideIt(div){
		if(bw.bw){
			div="carregapagina"
			obj=bw.dom?document.getElementById(div).style:bw.ie4?document.all[div].style:bw.ns4?document[div]:0; 
			obj.visibility='hidden'
		}
	}
	onload=hideIt;

	// carrega p�gina
	function carregapagina(url,target) {
		window.location.href=url;
	}
	
	// formatador campo
	function formataCampo(campo, mask, evt) { 
		if(document.all) { // Internet Explorer 
			key = evt.keyCode;
		} else { // Nestcape 
			key = evt.which; 
		} 
		string = campo.value;
		i = string.length;
		if (key != 08){
			if (i < mask.length) {
				if (mask.charAt(i) == '#') {
					return (key > 47 && key < 58);
				} else {
					if (mask.charAt(i) == '$') {
						return true;
					}
					for (c = i; c < mask.length; c++) {
						if (mask.charAt(c) != '#' && mask.charAt(c) != '$')
							campo.value = campo.value + mask.charAt(c);
						else if (mask.charAt(c) == '$') {
							return true;
						} else {
							return (key > 47 && key < 58);
						}
					}
				}
			} else
				return false;
		} else
				return (key);
	}
	
	// valida cpf
	function validaCPF(s) {
		var i;
		s = limpaCaracter(s,"0123456789");
		var c = s.substr(0,9);
		var dv = s.substr(9,2);
		var d1 = 0;
		for (i = 0; i < 9; i++)	{
			d1 += c.charAt(i)*(10-i);
		}
		
		if (d1 == 0) return false;
		d1 = 11 - (d1 % 11);
		if (d1 > 9) d1 = 0;
		if (dv.charAt(0) != d1) {
			return false;
		}
		
		d1 *= 2;
		for (i = 0; i < 9; i++) {
			d1 += c.charAt(i)*(11-i);
		}
		d1 = 11 - (d1 % 11);
		if (d1 > 9) d1 = 0;
		if (dv.charAt(1) != d1) {
			return false;
		}
		return true;
	}
	
	// bloqueia alfacaracter, somente numeral
	function bloqueiaAlfa(evnt) {
		if (navigator.appName.indexOf('Microsoft') != -1) {
			if (evnt.keyCode == 42) {
				return (evnt.keyCode);
			} else {
				if (evnt.keyCode < 48 || evnt.keyCode > 57) { return false; }
			}
		} else {
			if (evnt.charCode == 42) {
				return (evnt.charCode);
			} else {
				if ((evnt.charCode < 48 || evnt.charCode > 57) && evnt.keyCode == 0) { return false; }
			}
		}
	}
	
	// limpa caractar
	function limpaCaracter(valor, validos) {
		var result = "";
		var aux;
		for (var i=0; i < valor.length; i++) {
			aux = validos.indexOf(valor.substring(i, i+1));
			if (aux>=0) {
				result += aux;
			}
		}
		return result;
	}
	
	// formata valores monet�rio
	function formataCurrency(campo,tammax,teclapres,decimal) {
		var tecla = teclapres.keyCode;
		vr = limpaCaracter(campo.value,"0123456789");
		tam = vr.length;
		dec=decimal
		if (tam < tammax && tecla != 8){ tam = vr.length + 1 ; }
		if (tecla == 8 ) { tam = tam - 1; }
		if ( tecla == 8 || tecla >= 48 && tecla <= 57 || tecla >= 96 && tecla <= 105 ) {
			if ( tam <= dec ) { campo.value = vr ; }
			if ( (tam > dec) && (tam <= 5) ){ campo.value = vr.substr( 0, tam - 2 ) + "." + vr.substr( tam - dec, tam ); }
			if ( (tam >= 6) && (tam <= 8) ){ campo.value = vr.substr( 0, tam - 5 ) + "" + vr.substr( tam - 5, 3 ) + "." + vr.substr( tam - dec, tam ); }
			if ( (tam >= 9) && (tam <= 11) ){ campo.value = vr.substr( 0, tam - 8 ) + "" + vr.substr( tam - 8, 3 ) + "" + vr.substr( tam - 5, 3 ) + "." + vr.substr( tam - dec, tam ); }
			if ( (tam >= 12) && (tam <= 14) ){ campo.value = vr.substr( 0, tam - 11 ) + "" + vr.substr( tam - 11, 3 ) + "" + vr.substr( tam - 8, 3 ) + "" + vr.substr( tam - 5, 3 ) + "." + vr.substr( tam - dec, tam ); }
			if ( (tam >= 15) && (tam <= 17) ){ campo.value = vr.substr( 0, tam - 14 ) + "" + vr.substr( tam - 14, 3 ) + "" + vr.substr( tam - 11, 3 ) + "" + vr.substr( tam - 8, 3 ) + "" + vr.substr( tam - 5, 3 ) + "." + vr.substr( tam - 2, tam ); }
		} 
	}
	
	// exibe alerta
	function exibeAlerta(conf) {
		if (conf!="") {
			alert(conf);
			conf="";
			return false;
		}
	}
	
	// funcao que faz a troca de imagens ao deixar o mouse por cima
	function MM_swapImgRestore() { //v3.0
		var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
	}
	function MM_preloadImages() { //v3.0
		var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
		var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
		if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
	}
	function MM_findObj(n, d) { //v4.01
		var p,i,x;if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
			d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
		if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
		for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
		if(!x && d.getElementById) x=d.getElementById(n); return x;
	}
	function MM_swapImage() { //v3.0
		var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
		if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
	}
	
	// valida autentica��o
	function validaAutenticacao() {
		conf="";
		if (document.login.usuario.value=="") { conf=conf+"\n --> Usu�rio"; }
		if (document.login.senha.value=="") { conf=conf+"\n --> Senha"; }
		if (conf!="") {
			conf="Os seguintes dados est�o faltando:\n"+conf;
			alert(conf);
			conf="";
			return false;
		} else {
			document.login.post.value="enviando dados...";
			document.login.post.disabled="true";
			return true;
		}
	}
	
	// valida turma
	function validaTurma() {
		conf="";
		if (document.turma.id_curso.value=="") { conf=conf+"\n --> Curso"; }
		if (document.turma.id_codigoturma.value=="") { conf=conf+"\n --> Turma"; }
		if (document.turma.datainicio.value=="") { conf=conf+"\n --> In�cio das aulas"; }
		if (document.turma.datatermino.value=="") { conf=conf+"\n --> T�rmino das aulas"; }
		if (conf!="") {
			conf="Os seguintes dados est�o faltando:\n"+conf;
			alert(conf);
			conf="";
			return false;
		}
	}
	
	// subrotinas da cria��o de turma
	function removendoaula(idaula) {
		document.turma.idRemoveAula.value=idaula;
	}
	function adicionandoUsuario(idaddusuario) {
		document.turma.idAdicionaUsuario.value=idaddusuario;
	}
	function removendoUsuario(idremusuario) {
		document.turma.idRemoveUsuario.value=idremusuario;
	}
	
	// valida perfil de login
	function validaPerfildelogin() {
		conf="";
		if (document.perfildelogin.id_acessonivel.value=="") { conf=conf+"\n --> N�vel de acesso"; }
		if (document.perfildelogin.usuario.value=="") { conf=conf+"\n --> Usu�rio"; }
		if (document.perfildelogin.senha.value=="") { conf=conf+"\n --> Senha"; }
		if (conf!="") {
			conf="Os seguintes dados est�o faltando:\n"+conf;
			alert(conf);
			conf="";
			return false;
		} else {
			document.perfildelogin.post.value="enviando dados...";
			document.perfildelogin.post.disabled="true";
			return true;
		}
	}
	
	// valida consulta usuario
	function validaConsultaUsuario() {
		conf="";
		if (document.consulta.busca.value=="") { conf=conf+"\n --> Nome ou matr�cula"; }
		if (conf!="") {
			conf="Os seguintes dados est�o faltando:\n"+conf;
			alert(conf);
			conf="";
			return false;
		} else {
			document.consulta.post.value="enviando dados...";
			document.consulta.post.disabled="true";
			return true;
		}
	}	
	
	// valida cadastro de usuario
	function validaUsuario() {
		conf="";
		if (document.usuario.id_tipousuario.value=="") { conf=conf+"\n --> Tipo do usu�rio"; }
		if (document.usuario.nome.value=="") { conf=conf+"\n --> Nome completo"; }
		if (document.usuario.datanascimento.value=="") { conf=conf+"\n --> Data de nascimento"; }
		if (document.usuario.id_sexo.value=="") { conf=conf+"\n --> Sexo"; }
		if (document.usuario.id_nacionalidade.value=="") { conf=conf+"\n --> Nacionalidade"; }
		if (document.usuario.id_nacionalidade.value=="2") {
			if (document.usuario.paisorigem.value=="") { conf=conf+"\n --> Pais de origem"; }
		}
		if (document.usuario.id_nacionalidade.value=="3") {
			if (document.usuario.paisorigem.value=="") { conf=conf+"\n --> Pais de origem"; }
		}
		if (document.usuario.id_estadocivil.value=="") { conf=conf+"\n --> Estado Civil"; }
		if (document.usuario.id_raca.value=="") { conf=conf+"\n --> Ra�a/Etnia"; }
		if (document.usuario.enderecologradouro.value=="") { conf=conf+"\n --> Logradouro(nome da rua)";}
		if (document.usuario.endereconumero.value=="") { conf=conf+"\n --> N�mero da resid�ncia"; }
		if (document.usuario.endereco_id_bairro.value=="") { conf=conf+"\n --> Bairro"; }
		if (document.usuario.endereco_id_municipio.value=="") { conf=conf+"\n --> Munic�oio"; }
		if (document.usuario.endereco_id_uf.value=="") { conf=conf+"\n --> Estado(UF)"; }
		if (document.usuario.telefonefixo.value=="") { conf=conf+"\n --> Telefone fixo"; }
		if (document.usuario.cpf.value=="") { conf=conf+"\n --> CPF"; } else {
			if (document.usuario.cpf.value!="*") {
				if (validaCPF(document.usuario.cpf.value) == false ) {
					conf=conf+"\n --> CPF digitado n�o � v�lido!";
				}
			}
		}
		if (document.usuario.identidadenumero.value=="") { conf=conf+"\n --> Identidade(RG)"; }
		if (document.usuario.ctpsnumero.value=="") { conf=conf+"\n --> N�mero da Cateira Profissional(CTPS)"; }
		if (document.usuario.ctpsserie.value=="") { conf=conf+"\n --> S�rie da Cateira Profissional(CTPS)"; }
		if (document.usuario.tituloeleitornumero.value=="") { conf=conf+"\n --> N�mero do T�tulo de Eleitor"; }
		if (document.usuario.tituloeleitorzona.value=="") { conf=conf+"\n --> Zona do T�tulo de Eleitor"; }
		if (document.usuario.tituloeleitorsecao.value=="") { conf=conf+"\n --> Se��o do T�tulo de Eleitor"; }
		if (document.usuario.escola_id_escolatipo.value=="") { conf=conf+"\n --> Tipo da escola"; }
		if (document.usuario.escola_id_escolagrau.value=="") { conf=conf+"\n --> Grau escolar"; }
		if (document.usuario.escola_id_escolaserie.value=="") { conf=conf+"\n --> S�rie escolar"; }
		if (document.usuario.escola_id_escolaturno.value=="") { conf=conf+"\n --> Turno escolar"; }
		if (document.usuario.escola_id_escolanome.value=="") { conf=conf+"\n --> Escola"; }
		if (document.usuario.emprego_id_empregosituacao.value=="") { conf=conf+"\n --> Situa��o no mercado de trabalho"; }
		if (document.usuario.emprego_id_empregosituacao.value!="") {
			if (document.usuario.empregoempresa.value=="") { conf=conf+"\n --> Empresa"; }
		}
		if (conf!="") {
			conf="Os seguintes dados est�o faltando:\n"+conf;
			alert(conf);
			conf="";
			return false;
		} else {
			document.usuario.post.value="enviando dados...";
			document.usuario.post.disabled="true";
			return true;
		}
	}
	
	// valida cadastro de estagiario
	function validaEstagiario() {
		conf="";
		if (document.estagiario.nome.value=="") { conf=conf+"\n --> Nome completo"; }
		if (document.estagiario.datanascimento.value=="") { conf=conf+"\n --> Data de nascimento"; }
		if (document.estagiario.id_sexo.value=="") { conf=conf+"\n --> Sexo"; }
		if (document.estagiario.matricula.value=="") { conf=conf+"\n --> Matr�cula"; }
		if (document.estagiario.dataadmissao.value=="") { conf=conf+"\n --> Data de admiss�o"; }
		if (document.estagiario.horasaida.value=="") { conf=conf+"\n --> Hora de sa�da"; }
		if (document.estagiario.horaentrada.value=="") { conf=conf+"\n --> Hora de entrada"; }
		if (document.estagiario.id_telecentro.value=="") { conf=conf+"\n --> Unidade atuante"; }
		if (document.estagiario.id_estadocivil.value=="") { conf=conf+"\n --> Estado Civil"; }
		if (document.estagiario.enderecologradouro.value=="") { conf=conf+"\n --> Logradouro(nome da rua)";}
		if (document.estagiario.endereconumero.value=="") { conf=conf+"\n --> N�mero da resid�ncia"; }
		if (document.estagiario.endereco_id_bairro.value=="") { conf=conf+"\n --> Bairro"; }
		if (document.estagiario.endereco_id_municipio.value=="") { conf=conf+"\n --> Munic�oio"; }
		if (document.estagiario.endereco_id_uf.value=="") { conf=conf+"\n --> Estado(UF)"; }
		if (document.estagiario.telefonefixo.value=="") { conf=conf+"\n --> Telefone fixo"; }
		if (document.estagiario.cpf.value=="") { conf=conf+"\n --> CPF"; } else {
			if (document.estagiario.cpf.value!="*") {
				if (validaCPF(document.estagiario.cpf.value) == false ) {
					conf=conf+"\n --> CPF digitado n�o � v�lido!";
				}
			}
		}
		if (document.estagiario.identidadenumero.value=="") { conf=conf+"\n --> N�mero da Identidade(RG)"; }
		if (document.estagiario.ctpsnumero.value=="") { conf=conf+"\n --> N�mero da Cateira Profissional(CTPS)"; }
		if (document.estagiario.ctpsserie.value=="") { conf=conf+"\n --> S�rie da Cateira Profissional(CTPS)"; }
		if (document.estagiario.tituloeleitornumero.value=="") { conf=conf+"\n --> N�mero do T�tulo de Eleitor"; }
		if (document.estagiario.tituloeleitorzona.value=="") { conf=conf+"\n --> Zona do T�tulo de Eleitor"; }
		if (document.estagiario.tituloeleitorsecao.value=="") { conf=conf+"\n --> Se��o do T�tulo de Eleitor"; }
		if (document.estagiario.escola_id_escolatipo.value=="") { conf=conf+"\n --> Frequenta institui��o"; }
		if (document.estagiario.escola_id_escolagrau.value=="") { conf=conf+"\n --> Grau de instru��o"; }
		if (document.estagiario.escola_id_escolaserie.value=="") { conf=conf+"\n --> S�rie escolar"; }
		if (document.estagiario.escola_id_escolaturno.value=="") { conf=conf+"\n --> Turno"; }
		if (document.estagiario.escola_escolanome.value=="") { conf=conf+"\n --> Nome da institui��o"; }
		if (conf!="") {
			conf="Os seguintes dados est�o faltando:\n"+conf;
			alert(conf);
			conf="";
			return false;
		} else {
			document.estagiario.post.value="enviando dados...";
			document.estagiario.post.disabled="true";
			return true;
		}
	}